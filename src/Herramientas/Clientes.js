export default class Clientes {
    static listarClientes(clientes){
        let ListaClientes = []
        clientes.map(cliente =>{
            let elemento = {
                id: cliente.id,
                nombre: cliente.user.nombre + " " + cliente.user.apellido
            }
            ListaClientes.push(elemento)
            return null
        })
        return ListaClientes
    }

    static listarClientesFull(clientes){
        let ListaClientes = []
        clientes.map(cliente =>{
            let elemento = {
                id: cliente.id,
                nombre: cliente.user.nombre,
                apellido: cliente.user.apellido,
                email: cliente.user.email,
                nacimiento: cliente.nacimiento,
                telefono: cliente.telefono
            }
            ListaClientes.push(elemento)
            return null
        })
        return ListaClientes
    }

    static listarClientesReservasFull(reservas){
        let ListaReservas = []
        reservas.map(reserva =>{
            let elemento = {
                id: reserva.id,
                //fecha: reserva.fecha,
                inicio: reserva.inicio,
                fin: reserva.fin,
                nombre: reserva.cliente.user.nombre + " " + reserva.cliente.user.apellido,
                email: reserva.cliente.user.email,
                nacimiento: reserva.cliente.nacimiento,
                telefono: reserva.cliente.telefono
            }
            ListaReservas.push(elemento)
            return null
        })
        return ListaReservas
    }
}