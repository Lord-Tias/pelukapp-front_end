import Axios    from 'axios'

import {
    green,
    lightBlue,
    blue,
    lightGreen,
    purple,
    lime,
    orange
}               from '@material-ui/core/colors';

const Colores = [green,lightBlue,blue,lightGreen,purple,lime,orange]
function getColorFromId(id){
    const resto = id % Colores.length
    return Colores[resto]
}

export default class Peluqueros {
    static GetPeluqueros(){
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.get('getPeluqueros', config)
        .then(result => {
            
            return result.data.Peluqueros;
        })
        .catch( err => console.log(err));
    }

    static tituloPeluqueros(peluqueros){
        let ListaPeluqueros = []
        peluqueros.map(pelu =>{
            let ele = {
                id: pelu.id,
                text: pelu.user.nombre,
                color: getColorFromId(pelu.id)
            }
            ListaPeluqueros.push(ele)
            return null
        })
        return ListaPeluqueros
    }

    static listarPeluqueros(clientes){
        let ListaClientes = []
        clientes.map(cliente =>{
            let elemento = {
                id: cliente.id,
                nombre: cliente.user.nombre + " " + cliente.user.apellido
            }
            ListaClientes.push(elemento)
            return null
        })
        return ListaClientes
    }

    static listarPeluquerosFull(peluqueros){
        let ListaPeluqueros = []
        peluqueros.map(peluquero =>{
            let elemento = {
                id: peluquero.id,
                nombre: peluquero.user.nombre,
                apellido: peluquero.user.apellido,
                email: peluquero.user.email,
                inicio_actividad: peluquero.inicio_actividad,
                fin_actividad: peluquero.fin_actividad
            }
            ListaPeluqueros.push(elemento)
            return null
        })
        return ListaPeluqueros
    }
}
