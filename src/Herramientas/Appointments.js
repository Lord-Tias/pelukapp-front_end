export default class Appointments {
    
    static GetAppointments($datos){
        let $appoint = []
        $datos.forEach(element => {
            let $ele = {
                title: 'RESERVADO',
                peluId: element.peluquero_id,
                startDate: new Date((element.fecha.concat(" ").concat(element.inicio))),
                endDate: new Date((element.fecha.concat(" ").concat(element.fin))),
                id: element.id,
            }
            $appoint.push($ele)
        });
        return $appoint;
    }

    static GetAppointmentsPeluquero($datos){
        let $appoint = []
        $datos.forEach(element => {
            let $ele = {
                title: element.cliente.user.nombre.concat(" ").concat(element.cliente.user.apellido),
                peluId: element.peluquero_id,
                startDate: new Date((element.fecha.concat(" ").concat(element.inicio))),
                endDate: new Date((element.fecha.concat(" ").concat(element.fin))),
                id: element.id,
            }
            $appoint.push($ele)
        });
        return $appoint;
    }

    static GetAppointmentsCliente($datos){
        let $appoint = []
        $datos.forEach(element => {
            let $ele = {
                title: element.peluquero.user.nombre.concat(" ").concat(element.peluquero.user.apellido),
                peluId: element.peluquero_id,
                startDate: new Date((element.fecha.concat(" ").concat(element.inicio))),
                endDate: new Date((element.fecha.concat(" ").concat(element.fin))),
                id: element.id,
            }
            $appoint.push($ele)
        });
        return $appoint;
    }
}
