import React, {useEffect}   from 'react';
import Axios            from 'axios';

import ListItem         from '@material-ui/core/ListItem';
import ListItemIcon     from '@material-ui/core/ListItemIcon';
import ListItemText     from '@material-ui/core/ListItemText';
import AssignmentIcon   from '@material-ui/icons/Assignment';
import Typography       from '@material-ui/core/Typography';

import Cliente                  from '../../../Herramientas/Clientes';

import { makeStyles }   from '@material-ui/core/styles';
import Button           from '@material-ui/core/Button';
import Grid             from '@material-ui/core/Grid';

import { DataGrid }     from '@material-ui/data-grid';

import ConfirmDialog    from './Special_Components/Mensaje_Confirmacion'
import Alertas          from './Special_Components/Alerta_Eliminar';

import {
    DatePicker,
    MuiPickersUtilsProvider
}                           from "@material-ui/pickers";
import DateFnsUtils         from '@date-io/date-fns';
import esLocale             from "date-fns/locale/es";

const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
    boton: {
        margin: '7px'
    }
});

//Funcion de herramienta para formatear las fechas.
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

const columns = [
    {
        field: 'nombre',
        headerName: 'Nombre',
        description: 'Dia de la Licencia',
        sortable: false,
        flex: 1,
    },
    {
        field: 'inicio',
        headerName: 'Hora de inicio',
        description: 'Dia de la Licencia',
        sortable: false,
        flex: 1,
    },
    {
        field: 'email',
        headerName: 'Email',
        description: 'Dia de la Licencia',
        sortable: false,
        flex: 1,
    },
    {
        field: 'nacimiento',
        headerName: 'Nacimiento',
        description: 'Dia de la Licencia',
        sortable: false,
        flex: 1,
        type: 'date'
    },
    {
        field: 'telefono',
        headerName: 'Telefono',
        description: 'Dia de la Licencia',
        sortable: false,
        flex: 1,
    },
  ];

export function Mis_Horarios() {
    const classes = useStyles();
    const FechaHoy      = formatDate(new Date())
    const rol_info      = JSON.parse(sessionStorage.getItem('rol_info'))

    //Lista de Licencias.
    const [listaLicencias, setListaLicencias]   = React.useState([]);

    //Variables para cargar la fecha de la Reserva.
    const [diasLibres]                      = React.useState(rol_info.dias_nolaborables.split(","));
    const [selectedDate, handleDateChange]  = React.useState(new Date());
    const [fecha, setFecha]                 = React.useState(FechaHoy);
    const handleChangeFecha     = (date) => {
        if( ! isNaN(new Date(date))){
            setFecha(date);
            effectCargarReservas(date)
        }
    };
    const disableWeekends       = (date) => {
        return diasLibres.some(function(el) {return el === ""+date.getDay();});
    };

    //Alerta.
    const [exitoReserva, setExitoReserva]   = React.useState(false);
    const [open, setOpen]                   = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    const [select, setSelection]            = React.useState({rowIds:[]});
    const [confirmDialog, setConfirmDialog] = React.useState(
        {   isOpen: false,
            title: '',
            subTitle: '' })

    const deleteLicencias = (seleccion) => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                                    }
                            };

        let datos = {reservas_id: seleccion}
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('deleteMultiple', datos, config)
        .then(result => {
            if(result.data.Message === "ERROR"){
                setExitoReserva(false)
                abrirAlerta()
            }else{
                setExitoReserva(true)
                abrirAlerta()
                effectCargarReservas(fecha)
            }
        })
        .catch( err => console.log(err));
    }

    useEffect(() => {effectCargarReservas(fecha);}, []);

    const effectCargarReservas = (fecha) => {
        let token       = sessionStorage.getItem('token')
        let user        = JSON.parse(sessionStorage.getItem('user'))

        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos       = {
                            peluquero_id: user.id,
                            fecha: fecha
                        }
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('getPeluqueroReservasPorDia', datos, config)
        .then(result => {
            console.log(result.data)
            if(result.data.Message === "ERROR"){
                console.log(result.data)
            }else{
                console.log(result.data.Datos)
                setListaLicencias(Cliente.listarClientesReservasFull(result.data.Datos))
            }
        })
        .catch( err => {
            console.log(err)
        });
    }

    const handleEliminar = (seleccion) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        deleteLicencias(seleccion.rowIds)
    }

    return (
        <React.Fragment>
            <Grid container spacing={1} justify="center">
                <Alertas open={open} cerrar={CerrarAlerta} success={exitoReserva}/>
                <Grid item xl={4}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                        <DatePicker
                            shouldDisableDate={disableWeekends}
                            disablePast
                            autoOk
                            variant="inline"
                            inputVariant="outlined"
                            label="Fecha de busqueda"
                            invalidDateMessage="Fecha invalida. Formato: Año-Mes-Dia"
                            format="yyyy-MM-dd"
                            value={selectedDate}
                            onChange={date => {handleDateChange(date);handleChangeFecha(formatDate(date))}}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xl={4}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            if(select.rowIds.length > 0)
                                setConfirmDialog({
                                    isOpen: true,
                                    title: "¿Desea realizar la Eliminación?",
                                    subTitle: "No puedes deshacerlo luego de confirmar.",
                                    onConfirm: () => handleEliminar(select)
                                })
                        }}
                        className={classes.boton}
                    >
                        Eliminar Reserva
                    </Button>
                </Grid> 
                <Grid item xs={12}>
                    <div style={{ height: 450, width: '100%' }}>
                        <DataGrid 
                            rows={listaLicencias} 
                            columns={columns} 
                            pageSize={6} 
                            checkboxSelection 
                            onSelectionChange={(newSelection) => {
                                console.log(newSelection)
                                setSelection(newSelection);
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </React.Fragment>
    );
}

export function Mis_Horarios_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Mis_Horarios")}>
            <ListItemIcon>
                <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary="Mis Horarios" />
        </ListItem>
    )
}