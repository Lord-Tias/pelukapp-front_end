import React                from 'react'
import ListItem             from '@material-ui/core/ListItem';
import ListItemIcon         from '@material-ui/core/ListItemIcon';
import ListItemText         from '@material-ui/core/ListItemText';

import PersonAddRoundedIcon from '@material-ui/icons/PersonAddRounded';

import RegistroAdmin        from '../../Inicio/Registro_Admin'  

export function Alta_Administradores() {
    return (
        <React.Fragment>
            <RegistroAdmin Rol="Alta_Administradores"/>
        </React.Fragment>
    );
}

export function Alta_Administradores_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Alta_Administradores")}>
            <ListItemIcon>
                <PersonAddRoundedIcon />
            </ListItemIcon>
            <ListItemText primary="Crear Administrador" />
        </ListItem>
    )
}