import React, { useState }  from 'react'
import Axios                from 'axios'

import Button               from '@material-ui/core/Button';
import CssBaseline          from '@material-ui/core/CssBaseline';
import TextField            from '@material-ui/core/TextField';
import Grid                 from '@material-ui/core/Grid';
import Typography           from '@material-ui/core/Typography';
import Container            from '@material-ui/core/Container';

import 'react-phone-input-2/lib/material.css'

import ListItem             from '@material-ui/core/ListItem';
import ListItemIcon         from '@material-ui/core/ListItemIcon';
import ListItemText         from '@material-ui/core/ListItemText';
import { makeStyles }       from '@material-ui/core/styles';

import LockIcon             from '@material-ui/icons/Lock';

import Alertas              from './Special_Components/Alerta_Contraseña'
  
const useStyles = makeStyles((theme) => ({
    errorMsg: {
        marginLeft:         '14px',
        marginRight:        '14px',
        color:              '#f44336',
        fontSize:           '0.75rem'
    },
    paper: {
        marginTop:          theme.spacing(2),
        display:            'flex',
        flexDirection:      'column',
        alignItems:         'center',
    },
    avatar: {
        margin:             theme.spacing(1),
        //backgroundColor:    theme.palette.secondary.main,
    },
    form: {
        width:              '100%', // Fix IE 11 issue.
        marginTop:          theme.spacing(3),
    },
    submit: {
        margin:             theme.spacing(3, 0, 2),
    },
}));

export function Actualizar_Password() {
    //Instancia de estilos.
    const classes = useStyles();

    //Alerta.
    const [exitoReserva, setExitoReserva]   = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    //Estados.
    let [password,
        setPassword]                    = useState('')
    let [passwordError, 
        setPasswordError]               = useState('')
    let [contraseñaNueva, 
        setcontraseñaNueva]             = useState('')
    let [contraseñaNuevaError, 
        setcontraseñaNuevaError]         = useState('')
    let [contraseñaNueva_confirmation, 
        setcontraseñaNueva_Conf]         = useState('')

    //Instancias de Axios para consumir la API.
    let api
    if (process.env.NODE_ENV === 'production') {
        let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
        api             = Axios.create({baseURL : api_url.concat('/api/')})
    }else{
        api             = Axios.create({baseURL : '/api/'})
    }

    let token           = sessionStorage.getItem('token')
    let user           = JSON.parse(sessionStorage.getItem('user'))

    const config        = {
        headers: { Authorization: `Bearer ${token}`, Accept: 'application/json' }
    };

    //Se borran los mensajes de error.
    const cleanErrors = () => {
        setPasswordError(passwordError          = '')
        setcontraseñaNuevaError(contraseñaNuevaError   = '')
    }

    //Se colocan los mensajes de error.
    const setMensagesError = (Msg) => {
        cleanErrors()
        setPasswordError(passwordError  = Msg.password)
        setcontraseñaNuevaError(contraseñaNuevaError  = Msg.contraseñaNueva)
    }

    //Se borran los campos de registro
    const cleanCamposRegistro = () => {
        setPassword(password                    = '')
        setcontraseñaNueva(contraseñaNueva  = '')
        setcontraseñaNueva_Conf(contraseñaNueva_confirmation  = '')
    }
    

    //Se ejecuta un Post de registro a la API.
    const onSubmit = (e) => {
        e.preventDefault()
        RegFunction()
    };
    const RegFunction = async () => {
        let respuesta = await api.post(
            'CambioContraseña',
             {  
                email: user.email,
                password: password,
                contraseñaNueva:         contraseñaNueva,
                contraseñaNueva_confirmation:    contraseñaNueva_confirmation,
            },
            config)
        if(respuesta.data.Message === "ERROR"){
            setMensagesError(respuesta.data.Errores)
            setExitoReserva(false)
            abrirAlerta()
        }else{
            cleanCamposRegistro()
            cleanErrors()
            setExitoReserva(true)
            abrirAlerta()
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <form   className={classes.form}
                        onSubmit={onSubmit} autoComplete="off"
                >
                    <Grid container spacing={2} justify="center">
                        <Grid item xl={12}>
                            <Typography component="p" variant="h4" align='center'>
                                Actualizar Contraseña
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} justify="center">
                        <Alertas open={open} cerrar={CerrarAlerta} success={exitoReserva}/>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                name            = "password"
                                label           = "Contraseña actual"
                                type            = "password"
                                id              = "password"
                                onChange={(e) => setPassword(
                                    password = e.target.value
                                )}
                                onFocus={CerrarAlerta}
                                value={password}
                            />
                            <p className={classes.errorMsg}>
                                {passwordError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                name            = "contraseñaNueva"
                                label           = "Nueva contraseña"
                                type            = "password"
                                id              = "contraseñaNueva"
                                onChange={(e) => setcontraseñaNueva(
                                    contraseñaNueva = e.target.value
                                )}
                                onFocus={CerrarAlerta}
                                value={contraseñaNueva}
                            />
                            <p className={classes.errorMsg}>
                                {contraseñaNuevaError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                    variant         = "outlined"
                                    required
                                    fullWidth
                                    name            = "contraseñaNueva_confirmation"
                                    label           = "Confirmar nueva contraseña"
                                    type            = "password"
                                    id              = "contraseñaNueva_confirmation"
                                    onChange={(e) => setcontraseñaNueva_Conf(
                                        contraseñaNueva_confirmation = e.target.value
                                    )}
                                    onFocus={CerrarAlerta}
                                    value={contraseñaNueva_confirmation}
                                />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Actualizar
                    </Button>
                </form>
            </div>
        </Container>
    );
}

export function Actualizar_Password_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Actualizar_Password")}>
            <ListItemIcon>
                <LockIcon />
            </ListItemIcon>
            <ListItemText primary="Actualizar Password" />
        </ListItem>
    )
}