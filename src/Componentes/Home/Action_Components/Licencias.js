import React                from 'react'
import ListItem             from '@material-ui/core/ListItem';
import ListItemIcon         from '@material-ui/core/ListItemIcon';
import ListItemText         from '@material-ui/core/ListItemText';
import Typography           from '@material-ui/core/Typography';
import Grid                 from '@material-ui/core/Grid';

import GestionLicencias     from './Special_Components/Gestion_Licencias';
import LicenciaPeluquero    from './Special_Components/Licencia_Peluquero';

import BeachAccessIcon  from '@material-ui/icons/BeachAccess';

export function Licencias() {
    let user    = JSON.parse(sessionStorage.getItem('user'))

    const PerfilLoad = () => {
        switch (user.rol_id) {
            case 1:
                return(
                    <Grid item xl={12}>
                        <GestionLicencias />
                    </Grid>
                );
            case 2:
                return(
                    <Grid item xl={12}>
                        <LicenciaPeluquero />
                    </Grid>
                );
            default:
                return(null);
        }
    }

    return (
        <React.Fragment>
            <Grid container spacing={2} justify="center">
                <Grid item xl={12}>
                    <Typography component="p" variant="h4" align='center'>
                        Licencias
                    </Typography>
                </Grid>
                {PerfilLoad()}
            </Grid>
        </React.Fragment>
    );
}

export function Licencias_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Licencias")}>
            <ListItemIcon>
                <BeachAccessIcon />
            </ListItemIcon>
            <ListItemText primary="Licencias" />
        </ListItem>
    )
}