import React, { Component }     from 'react'
import Axios                    from 'axios'

import ListItem                 from '@material-ui/core/ListItem';
import ListItemIcon             from '@material-ui/core/ListItemIcon';
import ListItemText             from '@material-ui/core/ListItemText';

import Calendario               from './Special_Components/Calendario';
import ReservaEspecialSelector  from './Special_Components/Reserva_Especial_Selector';

import Appointments             from '../../../Herramientas/Appointments'
import Peluqueros               from '../../../Herramientas/Peluqueros'

import 
    PermContactCalendarRoundedIcon 
                                from '@material-ui/icons/PermContactCalendarRounded';

export class Reserva_Especial extends Component {

    state = {
        data: {
            Reservas: [],
            Peluqueros: [
                            { text: '', id: 1},
                            { text: '', id: 2}
                        ]
        }
    }

    constructor(){
        super();
        this.loadReservas()
    }

    loadReservas = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};

        //Instancia de Axios para conexion de API.
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getReservas', config)
        .then(result => {
            api.get('getPeluqueros', config)
                .then(pelu => {
                    this.setState({data: {
                        Reservas: Appointments.GetAppointments(result.data.Reservas),
                        Peluqueros: Peluqueros.tituloPeluqueros(pelu.data.Peluqueros)
                    }})
                })
                .catch( err => console.log(err));
        })
        .catch( err => console.log(err));
    }

    render() {
        return (
            <React.Fragment>
                <ReservaEspecialSelector loadCalendar={this.loadReservas}/>
                <Calendario Datos={this.state.data} key={Math.random()}/>
            </React.Fragment>
        )
    }
}

export function Reserva_Especial_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Reserva_Especial")}>
            <ListItemIcon>
                <PermContactCalendarRoundedIcon />
            </ListItemIcon>
            <ListItemText primary="Reservar Especial" />
        </ListItem>
    )
}