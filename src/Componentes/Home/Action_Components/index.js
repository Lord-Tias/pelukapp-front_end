export {Alta_Administradores_Item,  Alta_Administradores}
    from './Alta_Administradores';

export {Alta_Clientes_Item,         Alta_Clientes}
    from './Alta_Cliente';

export {Alta_Peluqueros_Item,       Alta_Peluqueros}
    from './Alta_Peluqueros';

//export {Configuraciones_Item,       Configuraciones}
//    from './Configuraciones';

//export {Estadisticas_Item,          Estadisticas}
//    from './Estadisticas';

export {Logout_Item,                Logout}
    from '../../Inicio/Logout';

export {Mis_Horarios_Item,          Mis_Horarios}
    from './Mis_Horarios';

export {Mis_Reservas_Item,          Mis_Reservas}
    from './Mis_Reservas';

export {Reserva_Especial_Item,      Reserva_Especial}
    from './Reserva_Especial';

export {Reserva_Item,               Reserva}
    from './Reserva';

export {Eliminar_Peluqueros_Item,   Eliminar_Peluqueros}
    from './Eliminar_Peluqueros';

export {Eliminar_Clientes_Item,     Eliminar_Clientes}
    from './Eliminar_Clientes';

export {Licencias_Item,             Licencias}
    from './Licencias';

export {Actualizar_Password_Item,   Actualizar_Password}
    from './Actualizar_Password';

export {Cambio_Password_Item,       Cambio_Password}
    from './Cambio_Password';

export {Home_Item,                  Home}
    from './Home';