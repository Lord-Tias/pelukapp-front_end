import React                    from 'react'

import ListItem                 from '@material-ui/core/ListItem';
import ListItemIcon             from '@material-ui/core/ListItemIcon';
import ListItemText             from '@material-ui/core/ListItemText';
import PersonAddOutlinedIcon    from '@material-ui/icons/PersonAddOutlined';

import RegistroPeluquero        from '../../Inicio/Registro_Peluquero'  

export function Alta_Peluqueros() {
    return (
        <React.Fragment>
            <RegistroPeluquero Rol="Alta_Peluqueros"/>
        </React.Fragment>
    );
}

export function Alta_Peluqueros_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Alta_Peluqueros")}>
            <ListItemIcon>
                <PersonAddOutlinedIcon />
            </ListItemIcon>
            <ListItemText primary="Registrar Peluqueros" />
        </ListItem>
    )
}