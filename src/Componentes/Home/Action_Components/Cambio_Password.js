import React            from 'react'

import ListItem         from '@material-ui/core/ListItem';
import ListItemIcon     from '@material-ui/core/ListItemIcon';
import ListItemText     from '@material-ui/core/ListItemText';
import Grid             from '@material-ui/core/Grid';
import Typography       from '@material-ui/core/Typography';

import LockOpenIcon     from '@material-ui/icons/LockOpen';

import CambioPasswordClientes   from './Special_Components/Cambio_Password_Clientes'
import CambioPasswordPeluqueros from './Special_Components/Cambio_Password_Peluqueros'

export function Cambio_Password() {
    return (
        <React.Fragment>
            <Grid container spacing={2} justify="center">
                <Grid item xl={6}>
                    <Typography component="p" variant="h4" align='center'>
                        Clientes
                    </Typography>
                    <CambioPasswordClientes  key={Math.random()}/>
                </Grid>
                <Grid item xl={6}>
                    <Typography component="p" variant="h4" align='center'>
                        Peluqueros
                    </Typography>
                    <CambioPasswordPeluqueros  key={Math.random()}/>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

export function Cambio_Password_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Cambio_Password")}>
            <ListItemIcon>
                <LockOpenIcon />
            </ListItemIcon>
            <ListItemText primary="Cambiar Passwords" />
        </ListItem>
    )
}