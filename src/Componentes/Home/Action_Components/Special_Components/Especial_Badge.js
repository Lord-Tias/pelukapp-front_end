import React                    from 'react'

import Badge                    from '@material-ui/core/Badge';
import PersonIcon               from '@material-ui/icons/Person';
import PermIdentityIcon         from '@material-ui/icons/PermIdentity';
import PermIdentityTwoToneIcon  from '@material-ui/icons/PermIdentityTwoTone';

export default function Especial_Badge() {

    let user    = JSON.parse(sessionStorage.getItem('user'))
    let rol     = user.rol.id

    const returnByRol = () => {
        if(rol === 1){
            return(
                <Badge color="secondary">
                    <PermIdentityTwoToneIcon />
                    Administrador
                </Badge>
            )
        }
        if(rol === 2){
            return(
                <Badge color="secondary">
                    <PermIdentityIcon />
                    Peluquero
                </Badge>
            )
        }
        if(rol === 3){
            return(
                <Badge color="secondary">
                    <PersonIcon />
                    Cliente
                </Badge>
            )
        }
    }

    return (
        returnByRol()
    )
}
