import React, { Component } from 'react';
import Axios                from 'axios';

import Appointments         from '../../../../Herramientas/Appointments'
import Peluqueros           from '../../../../Herramientas/Peluqueros'
import Calendario           from './Calendario';

export default class Home_Administrador extends Component {
    state = {
        data: {
            Reservas: [],
            Peluqueros: [
                            { text: '', id: 1},
                            { text: '', id: 2}
                        ]
        }
    }

    constructor(){
        super();
        this.loadReservas()
    }

    loadReservas = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};

        //Instancia de Axios para conexion de API.
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getReservas', config)
            .then(result => {
                console.log("Reservas: ", result.data.Reservas)
                api.get('getPeluqueros', config)
                    .then(pelu => {
                        console.log("Peluqueros: ", pelu.data.Peluqueros)
                        this.setState({data: {
                            Reservas: Appointments.GetAppointments(result.data.Reservas),
                            Peluqueros: Peluqueros.tituloPeluqueros(pelu.data.Peluqueros)
                        }})
                    })
                    .catch( err => console.log(err));
            })
            .catch( err => console.log(err));
    }

    render() {
        return (
            <React.Fragment>
                {console.log("Estado: ", this.state)}
                <Calendario Datos={this.state.data} key={Math.random()}/>
            </React.Fragment>
        )
    }
}