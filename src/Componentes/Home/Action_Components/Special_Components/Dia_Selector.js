import React, { useEffect } from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Days = [
    {nombre:'Domingo',      id:0},
    {nombre:'Lunes',        id:1},
    {nombre:'Martes',       id:2},
    {nombre:'Miercoles',    id:3},
    {nombre:'Jueves',       id:4},
    {nombre:'Viernes',      id:5},
    {nombre:'Sabado',       id:6}
];

function getDiasIds(dias){
    let ListaIds = []
    Days.map(Day =>{
        dias.map(dia =>{
            if(Day.nombre === dia) ListaIds.push(Day.id)
            return null
        })
        return null
    })
    return ListaIds
}
  
const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
});

function getStyles(name, dias, theme) {
    return {
      fontWeight:
        dias.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}

export default function Dia_Selector(props) {
    const classes                           = useStyles();
    const theme                             = useTheme();
    const [dias, setDias]                   = React.useState(["Domingo"]);

    //Efecto que se ejecuta el montar el componente.
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {props.setDias(getDiasIds(["Domingo"]))}, []);

    const handleChange = (event) => {
        props.setDias(getDiasIds(event.target.value))
        setDias(event.target.value);
    };

    return (
        <React.Fragment>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-mutiple-chip-label">Dias no laborables</InputLabel>
                <Select
                    style={{ width: 396 }}
                    labelId="demo-mutiple-chip-label"
                    id="demo-mutiple-chip"
                    multiple
                    value={dias}
                    onChange={handleChange}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={(selected) => (
                        <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip} />
                            ))}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                {Days.map((dia) => (
                    <MenuItem 
                        key={dia.id}
                        data-key={dia.id}
                        value={dia.nombre}
                        style={getStyles(dia.nombre, dias, theme)}
                    >
                        {dia.nombre}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
        </React.Fragment>
    );
}