import React, {useEffect}   from 'react';
import Axios                from 'axios';

import { makeStyles }       from '@material-ui/core/styles';
import Accordion            from '@material-ui/core/Accordion';
import AccordionDetails     from '@material-ui/core/AccordionDetails';
import AccordionSummary     from '@material-ui/core/AccordionSummary';
import Typography           from '@material-ui/core/Typography';
import ExpandMoreIcon       from '@material-ui/icons/ExpandMore';
import Grid                 from '@material-ui/core/Grid';
import Button               from '@material-ui/core/Button';
import TextField            from '@material-ui/core/TextField';

import Alertas              from './Alerta_Licencias'
import ListaLicencia        from './Lista_Licencias'

import {
    DatePicker,
    MuiPickersUtilsProvider
}                           from "@material-ui/pickers";
import DateFnsUtils         from '@date-io/date-fns';
import esLocale             from "date-fns/locale/es";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    cajas: {
        marginLeft: '5%',
        marginRight: '5%'
    },
    cajasTime: {
        marginTop: '15%',
        marginBottom: '15%'
    },
    errorMsg: {
        marginLeft:         '14px',
        marginRight:        '14px',
        color:              '#f44336',
        fontSize:           '0.75rem'
    },
}));

//Funcion de herramienta para formatear las fechas.
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

export default function Licencia_Peluquero() {
    const classes       = useStyles();
    const FechaHoy      = formatDate(new Date())
    const rol_info      = JSON.parse(sessionStorage.getItem('rol_info'))

    //Lista de Licencias.
    const [listaLicencias, setListaLicencias]   = React.useState([]);
    
    useEffect(() => {effectCargarLicencias();}, []);

    //Atributos de la Alerta.
    const [exitoLicencia, setExitoLicencia]   = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    //Atributos del Acordeon.
    const [expanded, setExpanded] = React.useState('panel3');
    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    //Atributos para mensajes de error.
    const [msgErrorRango, setMsgErrorRango]  = React.useState("");
    const limpiarMensajes = () => {
        setMsgErrorRango("")
        CerrarAlerta()
    }

    //Variables para cargar la fecha de la Reserva.
    const [diasLibres]                          = React.useState(rol_info.dias_nolaborables.split(","));
    const [selectedDateI, handleDateChangeI]    = React.useState(new Date());
    const [selectedDateF, handleDateChangeF]    = React.useState(new Date());
    const [fechaFin, setFechaFin]               = React.useState(FechaHoy);
    const handleChangeFechaF     = (date) => {
        if( ! isNaN(new Date(date))){
            console.log("Fecha Final: ", date)
            setFechaFin(date);
        }
        limpiarMensajes()
    };
    const [fecha, setFecha]                     = React.useState(FechaHoy);
    const handleChangeFechaI     = (date) => {
        if( ! isNaN(new Date(date))){
            console.log("Fecha Inicio: ", date)
            setFecha(date);
        }
        limpiarMensajes()
    };
    const disableWeekends       = (date) => {
        return diasLibres.some(function(el) {return el === ""+date.getDay();});
    };

    //Atributos de Hora
    let [inicio_actividad       , setInicio_Actividad]      = React.useState('')
    const handleInicio_Actividad = (value) => {
        setInicio_Actividad(value)
        limpiarMensajesError()
    }
    let [inicio_actividadError  , setInicio_ActividadError] = React.useState('')

    let [fin_actividad          , setFin_Actividad]         = React.useState('')
    const handleFin_Actividad = (value) => {
        setFin_Actividad(value)
        limpiarMensajesError()
    }
    let [fin_actividadError     , setFin_ActividadError]    = React.useState('')

    //Se colocan los mensajes de error.
    const setMensagesError = (Msg) => {
        setInicio_ActividadError(inicio_actividadError  = Msg.inicio)
        setFin_ActividadError(fin_actividadError        = Msg.fin)
    }
    const limpiarMensajesError = () => {
        setInicio_ActividadError("")
        setFin_ActividadError("")
    }


    const effectReservarLicenciaRango = () => {
        let token       = sessionStorage.getItem('token')
        let user        = JSON.parse(sessionStorage.getItem('user'))

        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos       = {
                            peluquero_id: user.id,
                            fechaInicio: fecha,
                            fechaFin: fechaFin
                        }
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('RegistrarLicenciaRango', datos, config)
        .then(result => {
            console.log(result.data)
            if(result.data.Message === "ERROR"){
                setExitoLicencia(false)
                abrirAlerta()
                setMsgErrorRango(result.data.Errores.fechaFin)
            }else{
                setExitoLicencia(true)
                abrirAlerta()
                effectCargarLicencias()
            }
        })
        .catch( err => {
            console.log(err)
            setExitoLicencia(false)
            abrirAlerta()
        });
    }

    const effectReservarLicencia = () => {
        let token       = sessionStorage.getItem('token')
        let user        = JSON.parse(sessionStorage.getItem('user'))

        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos       = {
                            peluquero_id: user.id,
                            fecha: fecha,
                            inicio: inicio_actividad,
                            fin: fin_actividad
                        }
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('RegistrarLicencia', datos, config)
        .then(result => {
            console.log(result.data)
            if(result.data.Message === "ERROR"){
                setExitoLicencia(false)
                abrirAlerta()
                setMensagesError(result.data.Errores)
            }else{
                setExitoLicencia(true)
                abrirAlerta()
                effectCargarLicencias()
            }
        })
        .catch( err => {
            console.log(err)
            setExitoLicencia(false)
            abrirAlerta()
        });
    }

    const effectCargarLicencias = () => {
        console.log("Entro")
        let token       = sessionStorage.getItem('token')
        let user        = JSON.parse(sessionStorage.getItem('user'))

        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos       = {
                            peluquero_id: user.id
                        }
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('getLicencias', datos, config)
        .then(result => {
            console.log(result.data)
            if(result.data.Message === "ERROR"){
                console.log(result.data)
            }else{
                setListaLicencias(result.data.Licencias)
            }
        })
        .catch( err => {
            console.log(err)
        });
    }


    const handleRangoLicencia = () => {
        effectReservarLicenciaRango()
    }
    const handleLicencia = () => {
        effectReservarLicencia()
    }

    return (
        <div className={classes.root}>
            {console.log("hola")}
            <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                <Typography className={classes.heading}>LICENCIA POR RANGO</Typography>
                <Typography className={classes.secondaryHeading}>
                    Licencia con una fecha de inicio y otra fecha final.
                </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Grid container spacing={2} justify="center" className={classes.dateAjuste}>
                        <Grid item xl={4} className={classes.cajas}>
                            <Typography component="p" variant="h4" align='center'>
                                Inicio
                            </Typography>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                <DatePicker
                                    shouldDisableDate={disableWeekends}
                                    disablePast
                                    autoOk
                                    variant="static"
                                    inputVariant="outlined"
                                    label="Fecha de Inicio"
                                    invalidDateMessage="Fecha invalida. Formato: Año-Mes-Dia"
                                    format="yyyy-MM-dd"
                                    value={selectedDateI}
                                    onChange={date => {handleDateChangeI(date);handleChangeFechaI(formatDate(date))}}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xl={4} className={classes.cajas}>
                            <Typography component="p" variant="h4" align='center'>
                                Fin
                            </Typography>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                <DatePicker
                                    shouldDisableDate={disableWeekends}
                                    disablePast
                                    autoOk
                                    variant="static"
                                    inputVariant="outlined"
                                    label="Fecha de Fin"
                                    invalidDateMessage="Fecha invalida. Formato: Año-Mes-Dia"
                                    format="yyyy-MM-dd"
                                    value={selectedDateF}
                                    onChange={date => {handleDateChangeF(date);handleChangeFechaF(formatDate(date))}}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xl={4} className={classes.cajas}>
                            <Alertas open={open} cerrar={CerrarAlerta} success={exitoLicencia}/>
                            <p className={classes.errorMsg}>
                                {msgErrorRango}</p>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={handleRangoLicencia}
                            >
                                Confirmar Licencia
                            </Button>
                        </Grid>
                    </Grid>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                <Typography className={classes.heading}>LICENCIA POR JORNADA</Typography>
                <Typography className={classes.secondaryHeading}>
                    Licencia por hora en jornada laboral.
                </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Grid container spacing={2} justify="center" className={classes.dateAjuste}>
                        <Grid item xl={4} className={classes.cajas}>
                            <Typography component="p" variant="h4" align='center'>
                                Inicio
                            </Typography>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                <DatePicker
                                    shouldDisableDate={disableWeekends}
                                    disablePast
                                    autoOk
                                    variant="static"
                                    inputVariant="outlined"
                                    label="Fecha de Inicio"
                                    invalidDateMessage="Fecha invalida. Formato: Año-Mes-Dia"
                                    format="yyyy-MM-dd"
                                    value={selectedDateI}
                                    onChange={date => {handleDateChangeI(date);handleChangeFechaI(formatDate(date))}}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xl={4} className={classes.cajas}>
                            <Typography component="p" variant="h4" align='center'>
                                Horario
                            </Typography>
                            <Grid item xs={12} className={classes.cajasTime}>
                                <TextField
                                    
                                    type            = "time"
                                    name            = "inicio_actividad"
                                    variant         = "outlined"
                                    required
                                    fullWidth
                                    id              = "inicio_actividad"
                                    label           = "Inicio de Licencia"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(e) => {handleInicio_Actividad(e.target.value)}}
                                    value={inicio_actividad}
                                />
                                <p className={classes.errorMsg}>
                                    {inicio_actividadError}</p>
                            </Grid>
                            <Grid item xs={12} className={classes.cajasTime}>
                                <TextField
                                    type            = "time"
                                    name            = "fin_actividad"
                                    variant         = "outlined"
                                    required
                                    fullWidth
                                    id              = "fin_actividad"
                                    label           = "Fin de Licencia"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(e) => {handleFin_Actividad(e.target.value)}}
                                    value={fin_actividad}
                                />
                                <p className={classes.errorMsg}>
                                    {fin_actividadError}</p>
                            </Grid>
                        </Grid>
                        <Grid item xl={4} className={classes.cajas}>
                            <Alertas open={open} cerrar={CerrarAlerta} success={exitoLicencia}/>
                            <p className={classes.errorMsg}>
                                {msgErrorRango}</p>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={handleLicencia}
                            >
                                Confirmar Licencia
                            </Button>
                        </Grid>
                    </Grid>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3bh-content"
                    id="panel3bh-header"
                >
                <Typography className={classes.heading}>GESTION DE LICENCIAS</Typography>
                <Typography className={classes.secondaryHeading}>
                    Lista de Licencias
                </Typography>
                </AccordionSummary>
                <AccordionDetails>
                <ListaLicencia  key={Math.random()} LoadTabla={() => effectCargarLicencias()} Licencias={listaLicencias}/>
                </AccordionDetails>
            </Accordion>
        </div>
    );
}