import React            from 'react';
import Axios            from 'axios';

import { makeStyles }   from '@material-ui/core/styles';
import Button           from '@material-ui/core/Button';
import Grid             from '@material-ui/core/Grid';

import { DataGrid }     from '@material-ui/data-grid';

import ConfirmDialog    from './Mensaje_Confirmacion'
import Alertas          from './Alerta_Eliminar';

const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
    boton: {
        margin: '7px'
    }
});

function obtenerTipoLicencia(hora) {
    if(hora === "00:00:00") return "Dia Completo"
    else return "Periodo"
}
function esDiaCompleto(hora){
    if(hora === "00:00:00" || hora === "23:59:00") return " - "
    else return hora
}
function parseCreated_at(fecha){
    return fecha.replace("T", " ").slice(0, 16)
}

const columns = [
    {
        field: 'tipo',
        headerName: 'Tipo de Licencia',
        description: 'El tipo de la Licencia.',
        sortable: true,
        flex: 1,
        valueGetter: (params) =>
          `${obtenerTipoLicencia(params.getValue('inicio'))}`,
    },
    {
        field: 'fecha',
        headerName: 'Dia',
        description: 'Dia de la Licencia',
        sortable: false,
        flex: 1,
        type: 'date'
    },
    {
        field: 'inicio-format',
        headerName: 'Hora de comienzo',
        description: 'Hora de inicio del periodo de Licencia',
        sortable: false,
        flex: 1,
        valueGetter: (params) =>
          `${esDiaCompleto(params.getValue('inicio'))}`,
    },
    {
        field: 'fin-format',
        headerName: 'Hora de fin',
        description: 'Hora de fin del periodo de Licencia',
        sortable: false,
        flex: 1,
        valueGetter: (params) =>
          `${esDiaCompleto(params.getValue('fin'))}`,
    },
    {
        field: 'created_at-format',
        headerName: 'Fecha de alta',
        description: 'Fecha en la que se dio de alta la Licencia.',
        sortable: false,
        flex: 1,
        valueGetter: (params) =>
          `${parseCreated_at(params.getValue('created_at'))}`,
    },
  ];

export default function Lista_Licencias(props) {
    const classes = useStyles();
    const {Licencias, LoadTabla} = props

    //Alerta.
    const [exitoReserva, setExitoReserva]   = React.useState(false);
    const [open, setOpen]                   = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    const [select, setSelection]            = React.useState({rowIds:[]});
    const [confirmDialog, setConfirmDialog] = React.useState(
        {   isOpen: false,
            title: '',
            subTitle: '' })

    const deleteLicencias = (seleccion) => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                                    }
                            };

        let datos = {reservas_id: seleccion}
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('deleteMultiple', datos, config)
        .then(result => {
            if(result.data.Message === "ERROR"){
                setExitoReserva(false)
                abrirAlerta()
            }else{
                setExitoReserva(true)
                abrirAlerta()
                LoadTabla()
            }
        })
        .catch( err => console.log(err));
    }

    const handleEliminar = (seleccion) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        deleteLicencias(seleccion.rowIds)
    }

    return (
        <React.Fragment>
            <Grid container spacing={1} justify="center">
                <Alertas open={open} cerrar={CerrarAlerta} success={exitoReserva}/>
                <Grid item xs={3}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            if(select.rowIds.length > 0)
                                setConfirmDialog({
                                    isOpen: true,
                                    title: "¿Desea realizar la Eliminación?",
                                    subTitle: "No puedes deshacerlo luego de confirmar.",
                                    onConfirm: () => handleEliminar(select)
                                })
                        }}
                        className={classes.boton}
                    >
                        Eliminar Licencia
                    </Button>
                </Grid> 
                <Grid item xs={12}>
                    <div style={{ height: 450, width: '100%' }}>
                        <DataGrid 
                            rows={Licencias} 
                            columns={columns} 
                            pageSize={6} 
                            checkboxSelection 
                            onSelectionChange={(newSelection) => {
                                console.log(newSelection)
                                setSelection(newSelection);
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </React.Fragment>
    );
}