import React, { useEffect } from 'react';
import Axios                from 'axios';

import { makeStyles }   from '@material-ui/core/styles';
import Grid                 from '@material-ui/core/Grid';

import Peluquero            from '../../../../Herramientas/Peluqueros';

import {
    DatePicker,
    MuiPickersUtilsProvider
}                           from "@material-ui/pickers";
import DateFnsUtils         from '@date-io/date-fns';
import esLocale             from "date-fns/locale/es";
import Autocomplete         from '@material-ui/lab/Autocomplete';
import TextField            from '@material-ui/core/TextField';
import FormControl          from '@material-ui/core/FormControl';

import ListaLicencia        from './Lista_Licencias'

const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
});

//Funcion de herramienta para formatear las fechas.
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

export default function Estadisticas() {
    const classes       = useStyles();
    const FechaHoy      = formatDate(new Date())

    //Lista de Licencias.
    const [listaLicencias, setListaLicencias]   = React.useState([]);

    //Variables para cargar la fecha de la Reserva.
    const [diasLibres, setDiasLibres]       = React.useState([0]);
    const [selectedDate, handleDateChange]  = React.useState(new Date());
    const [fecha, setFecha]                 = React.useState(FechaHoy);
    const handleChangeFecha     = (date) => {
        if( ! isNaN(new Date(date))){
            setFecha(date);
            effectCargarLicencias(peluquero, date)
        }
    };
    const handleChangeDiasLibres = (peluqueroId) => {
        let Dias_Libres = []
        peluquerosRaw.map(peluquero =>{
            if(peluquero.id === peluqueroId){
                setDiasLibres(peluquero.dias_nolaborables.split(","))
            }
            return null
        })
        return Dias_Libres
    }
    const disableWeekends       = (date) => {
        return diasLibres.some(function(el) {return el === ""+date.getDay();});
    };

    //Variables para la lista de peluqueros y el peluquero seleccionado.
    const [peluquerosRaw, setPeluqueroRaw]          = React.useState([]);
    const [peluquero, setPeluquero]                 = React.useState('');
    const [optionPeluquero, setOptionPeluquero]     = React.useState('');
    const [peluqueros, setPeluqueros]               = React.useState([{id:-1, nombre:""}]);
    const [inputPeluquero, setInputPeluquero]       = React.useState('');
    const handleChangePeluqueros        = (event, newValue) => {
        if (typeof newValue === 'string') {
            setOptionPeluquero({
                id:-1,
                nombre: newValue,
            });
            setPeluquero(null);
            handleChangeDiasLibres('');
        }else{
            setOptionPeluquero(newValue);
            if(newValue) {
                setPeluquero(newValue.id);
                handleChangeDiasLibres(newValue.id)
                effectCargarLicencias(newValue.id, fecha)
            }
            else {
                setPeluquero(null);
                handleChangeDiasLibres('');
            }
        }
    };
    const handlePeluqueroLabel    = (option) => {
        if (typeof option === 'string') {
            return option;
        }
        return option.nombre;
    };

    //Efecto que se ejecuta el montar el componente.
    //  Se cargan los Peluqueros.
    useEffect(() => {effectCargarPeluqueros();}, []);

    const effectCargarPeluqueros = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getPeluqueros', config)
        .then(result => {
            setPeluqueroRaw(result.data.Peluqueros);
            setPeluqueros(Peluquero.listarPeluqueros(result.data.Peluqueros));
        })
        .catch( err => console.log(err));
    }

    const effectCargarLicencias = (id, date) => {
        let token       = sessionStorage.getItem('token')

        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos       = {
                            peluquero_id: id,
                            fecha: date
                        }
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('getLicenciasPorFecha', datos, config)
        .then(result => {
            if(result.data.Message === "ERROR"){
                console.log(result.data)
            }else{
                setListaLicencias(result.data.Licencias)
            }
        })
        .catch( err => {
            console.log(err)
        });
    }


    return (
        <React.Fragment>
            <Grid container spacing={1} justify="center">
                <Grid item xl={4}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <Autocomplete
                            value={optionPeluquero}
                            onChange={handleChangePeluqueros}
                            inputValue={inputPeluquero}
                            onInputChange={(event, newInputValue) => {
                                setInputPeluquero(newInputValue);
                                }}
                            id="controllable-states-demo-pelu"
                            options={peluqueros}
                            getOptionLabel={handlePeluqueroLabel}
                            style={{ width: 300 }}
                            renderInput={(params) => <TextField {...params} label="Lista de Peluqueros" variant="outlined" />}
                        />
                    </FormControl>
                </Grid>
                <Grid item xl={4} className={classes.cajas}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                        <DatePicker
                            shouldDisableDate={disableWeekends}
                            disablePast
                            autoOk
                            variant="static"
                            inputVariant="outlined"
                            label="Fecha de Inicio"
                            invalidDateMessage="Fecha invalida. Formato: Año-Mes-Dia"
                            format="yyyy-MM-dd"
                            value={selectedDate}
                            onChange={date => {handleDateChange(date);handleChangeFecha(formatDate(date))}}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12} className={classes.cajas}>
                    <ListaLicencia  key={Math.random()} LoadTabla={() => effectCargarLicencias()} Licencias={listaLicencias}/>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}