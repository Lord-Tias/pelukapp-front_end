import React, { useEffect } from 'react';
import Axios                from 'axios';

import { makeStyles }       from '@material-ui/core/styles';
import Stepper              from '@material-ui/core/Stepper';
import Step                 from '@material-ui/core/Step';
import StepLabel            from '@material-ui/core/StepLabel';
import Button               from '@material-ui/core/Button';
import Select               from '@material-ui/core/Select';
import MenuItem             from '@material-ui/core/MenuItem';
import Paper                from '@material-ui/core/Paper';
import Grid                 from '@material-ui/core/Grid';
import TextField            from '@material-ui/core/TextField';
import Autocomplete         from '@material-ui/lab/Autocomplete';

import InputLabel           from '@material-ui/core/InputLabel';
import FormControl          from '@material-ui/core/FormControl';

import {
    DatePicker,
    MuiPickersUtilsProvider
}                           from "@material-ui/pickers";
import DateFnsUtils         from '@date-io/date-fns';
import esLocale             from "date-fns/locale/es";

import Alertas              from './Alerta_Reserva';

import Cliente              from '../../../../Herramientas/Clientes';
import Peluquero            from '../../../../Herramientas/Peluqueros';
import MensajeAlerta        from './Mensaje_Alerta'

//Estilos especificos del Selector.
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    margin: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  controles: {
      marginBottom: 15
  },
  paper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 287,
  },
  dateAjuste: {
      margin: 1,
      minWidth: 287,
  }
}));

//Funcion de herramienta para formatear las fechas.
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

//Detalla los pasos del selector.
function getSteps() {
  return ['Selecciona Peluquero', 'Seleccionar Cliente', 'Selecciona Fecha', 'Selecciona horario disponible', 'Confirma Reserva'];
}

//FUNCION PRINCIPAL
export default function Reserva_Especial_Selector(props) {
    const classes    = useStyles();
    const FechaHoy   = formatDate(new Date())

    //Alerta.
    const [exitoReserva, setExitoReserva]   = React.useState(false);
    const [open, setOpen]                   = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }
    //Mensajes.
    const [mensajeAlerta, setMensajeAlerta] = React.useState(
        {   isOpen: false,
            title: '',
            subTitle: '' })

    //Variables para el funcionamiento del selector.
    const [activeStep, setActiveStep]   = React.useState(0);
    const steps                         = getSteps();
    
    //Variables para la lista de peluqueros y el peluquero seleccionado.
    const [peluquerosRaw, setPeluqueroRaw]          = React.useState([]);
    const [peluquero, setPeluquero]                 = React.useState('');
    const [optionPeluquero, setOptionPeluquero]     = React.useState('');
    const [peluqueros, setPeluqueros]               = React.useState([{id:-1, nombre:""}]);
    const [inputPeluquero, setInputPeluquero]       = React.useState('');
    const handleChangePeluqueros        = (event, newValue) => {
        if (typeof newValue === 'string') {
            setOptionPeluquero({
                id:-1,
                nombre: newValue,
            });
            setPeluquero(null);
            setHorario('')
            setHorarios([])
            handleChangeDiasLibres('');
        }else{
            setOptionPeluquero(newValue);
            if(newValue) {
                effectCargarHorarios(newValue.id, fecha)
                setPeluquero(newValue.id);
                handleChangeDiasLibres(newValue.id)
            }
            else {
                setPeluquero(null);
                setHorario('')
                setHorarios([])
                handleChangeDiasLibres('');
            }
        }
    };
    const handlePeluqueroLabel    = (option) => {
        if (typeof option === 'string') {
          return option;
        }
        return option.nombre;
    };

    //Variables para listar Clientes.
    const [cliente, setCliente]             = React.useState('');
    const [optionCliente, setOptionCliente] = React.useState('');
    const [clientes, setClientes]           = React.useState([{id:-1, nombre:""}]);
    const [inputCliente, setInputCliente]   = React.useState('');
    const handleChangeCliente      = (event, newValue) => {
        if (typeof newValue === 'string') {
            setOptionCliente({
                id:-1,
                nombre: newValue,
            });
            setCliente(null);
        }else{
            setOptionCliente(newValue);
            if(newValue) setCliente(newValue.id);
            else setCliente(null);
        }
    };
    const handleClienteLabel    = (option) => {
        if (typeof option === 'string') {
          return option;
        }
        return option.nombre;
    };

    //Variables para la lista de horarios y el horario seleccionado.
    const [horario, setHorario]     = React.useState('');
    const [horarios, setHorarios]   = React.useState([]);
    const handleChangeHorarios      = (event) => {
        console.log("Horario: ", event.target.value)
        setHorario(event.target.value);
    };

    //Variables para cargar la fecha de la Reserva.
    const [diasLibres, setDiasLibres]       = React.useState([0]);
    const [selectedDate, handleDateChange]  = React.useState(new Date());
    const [fecha, setFecha]                 = React.useState(FechaHoy);
    const handleChangeFecha     = (date) => {
        if( ! isNaN(new Date(date))){
            console.log("Fecha: ", date)
            setFecha(date);
            effectCargarHorarios(peluquero, date)
        }
    };
    const handleChangeDiasLibres = (peluqueroId) => {
        let Dias_Libres = []
        console.log("PeluqueroID: ", peluqueroId)
        peluquerosRaw.map(peluquero =>{
            console.log("Peluquero: ", peluquero)
            if(peluquero.id === peluqueroId){
                console.log("Entro!", peluquero.dias_nolaborables.split(","))
                setDiasLibres(peluquero.dias_nolaborables.split(","))
            }
            return null
        })
        return Dias_Libres
    }
    const disableWeekends       = (date) => {
        return diasLibres.some(function(el) {return el === ""+date.getDay();});
    };


    //Efecto que se ejecuta el montar el componente.
    //  Se cargan los Peluqueros.
    useEffect(() => {effectCargarPeluqueros(); effectCargarClientes();}, []);

    const effectCargarPeluqueros = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getPeluqueros', config)
        .then(result => {
            setPeluqueroRaw(result.data.Peluqueros);
            setPeluqueros(Peluquero.listarPeluqueros(result.data.Peluqueros));
        })
        .catch( err => console.log(err));
    }

    const effectCargarClientes = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getClientes', config)
        .then(result => {
            setClientes(Cliente.listarClientes(result.data.Clientes));
        })
        .catch( err => console.log(err));
    }

    //Efecto que se ejecuta luego de seleccionar una fecha.
    //  Se cargan los horarios disponibles de los Peluqueros.
    const effectCargarHorarios = (id, date) => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos
        if(id){
            datos     = {id_peluquero: id, fecha: date}
        }else{
            datos     = {id_peluquero: peluquero, fecha: fecha}
        }
        
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        console.log(datos, config)
        api.post('getHorasLibres', datos, config)
        .then(result => {
            setHorarios(result.data.Datos.Blocks);
        })
        .catch( err => console.log(err));
    }

    const effectReservar = () => {
        let token       = sessionStorage.getItem('token')

        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                            }};
        let datos       = {
                            peluquero_id: peluquero,
                            cliente_id: cliente,
                            fecha: fecha,
                            inicio: horario
                        }
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        console.log(datos, config)

        api.post('registroReserva', datos, config)
        .then(result => {
            if(result.data.Message === "ERROR"){
                setExitoReserva(false)
                abrirAlerta()
            }else{
                setExitoReserva(true)
                abrirAlerta()
                props.loadCalendar()
                effectCargarHorarios()
            }
        })
        .catch( err => {
            console.log(err)
            setExitoReserva(false)
            abrirAlerta()
        });
    }


    //Selector de etapas.
    const getStepContent = (step) => {
        switch (step) {
            case 0:
                return(
                    <Grid container spacing={1} justify="center">
                        <Grid item xs={3}>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <Autocomplete
                                    value={optionPeluquero}
                                    onChange={handleChangePeluqueros}
                                    inputValue={inputPeluquero}
                                    onInputChange={(event, newInputValue) => {
                                        setInputPeluquero(newInputValue);
                                        }}
                                    id="controllable-states-demo-pelu"
                                    options={peluqueros}
                                    getOptionLabel={handlePeluqueroLabel}
                                    style={{ width: 300 }}
                                    renderInput={(params) => <TextField {...params} label="Lista de Peluqueros" variant="outlined" />}
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                );
            case 1:
                return(
                    <Grid container spacing={1} justify="center">
                        <Grid item xs={3}>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <Autocomplete
                                    value={optionCliente}
                                    onChange={handleChangeCliente}
                                    inputValue={inputCliente}
                                    onInputChange={(event, newInputValue) => {
                                        setInputCliente(newInputValue);
                                        }}
                                    id="controllable-states-demo-clie"
                                    options={clientes}
                                    getOptionLabel={handleClienteLabel}
                                    style={{ width: 300 }}
                                    renderInput={(params) => <TextField {...params} label="Lista de Clientes" variant="outlined" />}
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                );
            case 2:
                return(
                    <Grid container spacing={2} justify="center" className={classes.dateAjuste}>
                        <Grid item xs={3}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                                <DatePicker
                                    shouldDisableDate={disableWeekends}
                                    disablePast
                                    autoOk
                                    variant="inline"
                                    inputVariant="outlined"
                                    label="Fecha de Reserva"
                                    invalidDateMessage="Fecha invalida. Formato: Año-Mes-Dia"
                                    format="yyyy-MM-dd"
                                    value={selectedDate}
                                    onChange={date => {handleDateChange(date);handleChangeFecha(formatDate(date))}}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                );
            case 3:
                return(
                    <Grid container spacing={1} justify="center">
                        <Grid item xs={3}>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel id="demo-simple-select-outlined-label">
                                    Horarios disponibles
                                </InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={horario}
                                    onChange={handleChangeHorarios}
                                    label='Horarios disponibles'
                                >
                                    {horarios.map(elemento=>(
                                        <MenuItem   key={elemento} 
                                                    value={elemento}
                                        >
                                            {elemento}
                                        </MenuItem>
                                        )
                                    )}
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                );
            case 4:
                return(
                    <Grid container spacing={1} justify="center">
                        <Grid item xs={3}>
                            <Paper className={classes.paper}>
                                <b>{peluqueros.map(ele => {
                                        if(ele.id === peluquero)
                                            return ele.nombre;
                                        else return "";
                                    })
                                }</b> el dia <b>{fecha}</b> a las <b>{horario}</b> con <b>{
                                    clientes.map(ele => {
                                        if(ele.id === cliente)
                                            return ele.nombre;
                                        else return "";
                                    })}</b>
                            </Paper>
                        </Grid>
                    </Grid>
                );
            case 5:
                return(
                    <Grid container spacing={1} justify="center">
                        <Grid item xs={3}>
                            <div className={classes.controles}>
                                <Button 
                                    onClick={handleReset}
                                    className={classes.button}
                                >
                                    Reiniciar / Realizar otra reserva
                                </Button>
                            </div>
                        </Grid>
                    </Grid>
                );
            default:
                return 'Unknown step';
        }
    }


    const isStepFailed = (step) => {
        if(activeStep > 0 && step === 0 && !peluquero){
            return true
        }
        if(activeStep > 1 && step === 1 && !cliente){
            return true
        }
        if(activeStep > 2 && step === 2 && !fecha){
            return true
        }
        if(activeStep > 3 && step === 3 && !horario){
            return true
        }
        return false
    };

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        if(activeStep === 4){
            if(!peluquero || !cliente || !fecha || !horario){
                setMensajeAlerta({
                    isOpen: true,
                    title: 'Falta completar campos!',
                    subTitle: "No se puede realizar la reserva si no se completan todos los campos"
                })
                setActiveStep((prevActiveStep) => prevActiveStep - 1);
            }else{
                effectReservar()
            }
        }
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
        setHorario('')
        CerrarAlerta()
    };

    return (
        <Paper className={classes.root}>
            <Alertas open={open} cerrar={CerrarAlerta} success={exitoReserva}/>
            <MensajeAlerta
                confirmDialog={mensajeAlerta}
                setConfirmDialog={setMensajeAlerta}
            />
            <Stepper activeStep={activeStep}>
                {steps.map((label, index) => {
                    const stepProps = {};
                    const labelProps = {};
                    if (isStepFailed(index)) {
                        labelProps.error = true;
                    }
                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            <div>
                {activeStep === steps.length ? (
                    <div>
                        {getStepContent(5)}
                    </div>
                ) : (
                    <div>
                        {getStepContent(activeStep)}
                        <div className={classes.controles}>
                            <Button 
                                disabled={activeStep === 0}
                                onClick={handleBack}
                                className={classes.button}
                            >
                                Atras
                            </Button>

                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleNext}
                                className={classes.button}
                            >
                                {activeStep === steps.length - 1 ? 'Confirmar Reserva' : 'Siguiente'}
                            </Button>
                        </div>
                    </div>
                )}
            </div>
        </Paper>
    );
}
