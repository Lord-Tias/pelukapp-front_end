import React,
    { useEffect, useState } from 'react';
import Axios                from 'axios'

import Button               from '@material-ui/core/Button';
import CssBaseline          from '@material-ui/core/CssBaseline';
import TextField            from '@material-ui/core/TextField';
import Grid                 from '@material-ui/core/Grid';
import Container            from '@material-ui/core/Container';

import 'react-phone-input-2/lib/material.css'

import { makeStyles }       from '@material-ui/core/styles';

import Autocomplete         from '@material-ui/lab/Autocomplete';
import FormControl          from '@material-ui/core/FormControl';

import Alertas              from './Alerta_Contraseña'
import MensajeAlerta        from './Mensaje_Alerta'
import Peluquero            from '../../../../Herramientas/Peluqueros';
  
const useStyles = makeStyles((theme) => ({
    errorMsg: {
        marginLeft:         '14px',
        marginRight:        '14px',
        color:              '#f44336',
        fontSize:           '0.75rem'
    },
    paper: {
        marginTop:          theme.spacing(2),
        display:            'flex',
        flexDirection:      'column',
        alignItems:         'center',
    },
    avatar: {
        margin:             theme.spacing(1),
        //backgroundColor:    theme.palette.secondary.main,
    },
    form: {
        width:              '100%', // Fix IE 11 issue.
        marginTop:          theme.spacing(3),
    },
    formControl: {
        marginBottom:'3%',
        minWidth: 287,
    },
    submit: {
        margin:             theme.spacing(3, 0, 2),
    },
}));

export default function Cambio_Password_Peluqueros() {
    //Instancia de estilos.
    const classes = useStyles();

    //Efecto que se ejecuta el montar el componente.
    //  Se cargan los Peluqueros.
    useEffect(() => {effectCargarPeluqueros()}, []);

    //Mensajes.
    const [mensajeAlerta, setMensajeAlerta] = React.useState(
        {   isOpen: false,
            title: '',
            subTitle: '' })
    //Alerta.
    const [exitoUpdate, setExitoReserva]   = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    //Estados.
    let [contraseñaNueva, 
        setcontraseñaNueva]             = useState('')
    let [contraseñaNuevaError, 
        setcontraseñaNuevaError]         = useState('')
    let [contraseñaNueva_confirmation, 
        setcontraseñaNueva_Conf]         = useState('')
    
    //Variables para la lista de peluqueros y el peluquero seleccionado.
    const [peluquero, setPeluquero]                 = React.useState('');
    const [optionPeluquero, setOptionPeluquero]     = React.useState('');
    const [peluqueros, setPeluqueros]               = React.useState([{id:-1, nombre:""}]);
    const [inputPeluquero, setInputPeluquero]       = React.useState('');
    const handleChangePeluqueros        = (event, newValue) => {
        CerrarAlerta()
        if (typeof newValue === 'string') {
            setOptionPeluquero({
                id:-1,
                nombre: newValue,
            });
            setPeluquero(null);
        }else{
            setOptionPeluquero(newValue);
            if(newValue) setPeluquero(newValue.id);
            else setPeluquero(null);
        }
    };
    const handlePeluqueroLabel    = (option) => {
        if (typeof option === 'string') {
            return option;
        }
        return option.nombre;
    };
    const effectCargarPeluqueros = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getPeluqueros', config)
        .then(result => {
            setPeluqueros(Peluquero.listarPeluqueros(result.data.Peluqueros));
        })
        .catch( err => console.log(err));
    }

    //Instancias de Axios para consumir la API.
    let api
    if (process.env.NODE_ENV === 'production') {
        let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
        api             = Axios.create({baseURL : api_url.concat('/api/')})
    }else{
        api             = Axios.create({baseURL : '/api/'})
    }

    let token           = sessionStorage.getItem('token')

    const config        = {
        headers: { Authorization: `Bearer ${token}`, Accept: 'application/json' }
    };

    //Se borran los mensajes de error.
    const cleanErrors = () => {
        setcontraseñaNuevaError(contraseñaNuevaError   = '')
    }

    //Se colocan los mensajes de error.
    const setMensagesError = (Msg) => {
        cleanErrors()
        setcontraseñaNuevaError(contraseñaNuevaError  = Msg.contraseñaNueva)
    }

    //Se borran los campos de registro
    const cleanCamposRegistro = () => {
        setcontraseñaNueva(contraseñaNueva  = '')
        setcontraseñaNueva_Conf(contraseñaNueva_confirmation  = '')
    }
    

    //Se ejecuta un Post de registro a la API.
    const onSubmit = (e) => {
        e.preventDefault()
        RegFunction()
    };
    const RegFunction = async () => {
        if(!peluquero){
            setMensajeAlerta({
                isOpen: true,
                title: 'Falta elegir un Peluquero!',
                subTitle: "Debe elegir un Peluquero de la lista al cual quiera actualizar la contraseña."
            })
        }else{
            let respuesta = await api.post(
                'CambioContraseñaAdmin',
                {  
                    usuario_id: peluquero,
                    contraseñaNueva:         contraseñaNueva,
                    contraseñaNueva_confirmation:    contraseñaNueva_confirmation,
                },
                config)
            if(respuesta.data.Message === "ERROR"){
                setMensagesError(respuesta.data.Errores)
                setExitoReserva(false)
                abrirAlerta()
            }else{
                cleanCamposRegistro()
                cleanErrors()
                setExitoReserva(true)
                abrirAlerta()
            }
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <form   className={classes.form}
                        onSubmit={onSubmit} autoComplete="off"
                >
                    <Grid container spacing={2}>
                        <MensajeAlerta
                            confirmDialog={mensajeAlerta}
                            setConfirmDialog={setMensajeAlerta}
                        />
                        <Grid item xs={12}>
                            <Alertas open={open} cerrar={CerrarAlerta} success={exitoUpdate}/>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl variant="outlined" className={classes.formControl}>
                            <Autocomplete
                                    value={optionPeluquero}
                                    onChange={handleChangePeluqueros}
                                    inputValue={inputPeluquero}
                                    onInputChange={(event, newInputValue) => {
                                        setInputPeluquero(newInputValue);
                                        }}
                                    id="controllable-states-demo-pelu"
                                    options={peluqueros}
                                    getOptionLabel={handlePeluqueroLabel}
                                    style={{ width: 396 }}
                                    renderInput={(params) => <TextField {...params} label="Lista de Peluqueros" variant="outlined" />}
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                name            = "contraseñaNuevaP"
                                label           = "Nueva contraseña"
                                type            = "password"
                                id              = "contraseñaNuevaP"
                                onChange={(e) => setcontraseñaNueva(
                                    contraseñaNueva = e.target.value
                                )}
                                onFocus={CerrarAlerta}
                                value={contraseñaNueva}
                            />
                            <p className={classes.errorMsg}>
                                {contraseñaNuevaError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                    variant         = "outlined"
                                    required
                                    fullWidth
                                    name            = "contraseñaNueva_confirmationP"
                                    label           = "Confirmar nueva contraseña"
                                    type            = "password"
                                    id              = "contraseñaNueva_confirmationP"
                                    onChange={(e) => setcontraseñaNueva_Conf(
                                        contraseñaNueva_confirmation = e.target.value
                                    )}
                                    onFocus={CerrarAlerta}
                                    value={contraseñaNueva_confirmation}
                                />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Actualizar
                    </Button>
                </form>
            </div>
        </Container>
    );
}