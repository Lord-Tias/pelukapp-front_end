import * as React   from 'react';
import Paper        from '@material-ui/core/Paper';

import {
    ViewState,
    GroupingState,
    IntegratedGrouping,
}                   from '@devexpress/dx-react-scheduler';

import {
    Scheduler,
    Resources,
    Appointments,
    AppointmentTooltip,
    AppointmentForm,
    GroupingPanel,
    WeekView,
    Toolbar,
    DateNavigator,
    TodayButton,
}                   from '@devexpress/dx-react-scheduler-material-ui';


//Funcion de herramienta para formatear las fechas.
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

export default class Calendario extends React.Component {
    constructor(props) {
        super(props);
        this.state  = {
                        data: props.Datos.Reservas,
                        resources:  [{
                                        fieldName:  'peluId',
                                        title:      'Peluqueros',
                                        instances:  props.Datos.Peluqueros,
                                    }],
                        grouping: [{
                            resourceName:   'peluId',
                        }],
                        fechaHoy: formatDate(new Date())
                    };
    }

    render() {
        const {data, resources, grouping, fechaHoy} = this.state;
        return (
        <React.Fragment>
            <Paper>
            <Scheduler
                locale="es-ES"
                data={data}
                height={660}>

                <ViewState      defaultCurrentDate={fechaHoy}/>

                <GroupingState  grouping={grouping}
                                groupByDate={(viewName) => viewName === 'Week'}/>

                <WeekView       startDayHour={9}
                                endDayHour={18}
                                excludedDays={[0]}/>
                <Toolbar />
                <DateNavigator />
                <TodayButton />

                <Appointments />

                <Resources      data={resources}
                                mainResourceName="peluId"/>

                <IntegratedGrouping />

                <AppointmentTooltip />

                <AppointmentForm />

                <GroupingPanel />

            </Scheduler>
            </Paper>
        </React.Fragment>
        );
  }
}