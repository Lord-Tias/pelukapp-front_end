import React, { useEffect } from 'react';
import Axios                from 'axios';

import {
    ViewState
}                           from '@devexpress/dx-react-scheduler';

import {
    Scheduler,
    Appointments,
    AppointmentTooltip,
    AppointmentForm,
    WeekView,
    Toolbar,
    DateNavigator,
    TodayButton,
}                           from '@devexpress/dx-react-scheduler-material-ui';

import Reservas             from '../../../../Herramientas/Appointments'

//Funcion de herramienta para formatear las fechas.
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

export default function Home_Peluquero() {
    const fechaHoy                  = formatDate(new Date())
    const [reservas, setReservas]   = React.useState([]);

    //Efecto que se ejecuta el montar el componente.
    //  Se cargan los Peluqueros.
    useEffect(() => {loadReservas()}, []);

    const loadReservas = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};

        //Instancia de Axios para conexion de API.
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.get('getPeluqueroReservas', config)
        .then(result => {
            setReservas(
                Reservas.GetAppointmentsPeluquero(result.data.Datos)
            )
        })
        .catch( err => console.log(err));
    }

    return (
        <Scheduler
            locale="es-ES"
            data={reservas}
            height={660}>

            <ViewState 
                defaultCurrentDate={fechaHoy}
            />

            <WeekView       
                startDayHour={9}
                endDayHour={18}
                excludedDays={[0]}
            />

            <Toolbar />
            <DateNavigator />
            <TodayButton />
            <Appointments />
            <AppointmentTooltip />
            <AppointmentForm />
        </Scheduler>
    )
}