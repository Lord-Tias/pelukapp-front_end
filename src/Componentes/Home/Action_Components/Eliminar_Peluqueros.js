import React, { useEffect }     from 'react';
import Axios                    from 'axios';

import ListItem                 from '@material-ui/core/ListItem';
import ListItemIcon             from '@material-ui/core/ListItemIcon';
import ListItemText             from '@material-ui/core/ListItemText';
import { makeStyles }           from '@material-ui/core/styles';
import Button                   from '@material-ui/core/Button';
import Grid                     from '@material-ui/core/Grid';

import { DataGrid }             from '@material-ui/data-grid';
import PeopleAltIcon            from '@material-ui/icons/PeopleAlt';

import Peluquero                from '../../../Herramientas/Peluqueros';
import ConfirmDialog            from './Special_Components/Mensaje_Confirmacion'
import Alertas                  from './Special_Components/Alerta_Eliminar';
  
const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
    boton: {
        margin: '7px'
    }
});

const columns = [
    {
        field: 'Fullnombre',
        headerName: 'Nombre completo',
        description: 'Nombre completo del Cliente.',
        sortable: true,
        flex: 1,
        valueGetter: (params) =>
          `${params.getValue('nombre') || ''} ${params.getValue('apellido') || ''}`,
    },
    {
        field: 'email',
        headerName: 'Email',
        description: 'Correo electronico del Cliente.',
        sortable: false,
        flex: 1,
    },
    {
        field: 'inicio_actividad',
        headerName: 'Hora de Inicio',
        description: 'Hora de inicio de la jornada laboral.',
        sortable: false,
        flex: 1,
    },
    {
        field: 'fin_actividad',
        headerName: 'Hora de Fin',
        description: 'Hora de fin de la jornada laboral.',
        sortable: true,
        flex: 1,
        type: 'date'
    },
  ];

export function Eliminar_Peluqueros() {
    const classes = useStyles();

    //Alerta.
    const [exitoReserva, setExitoReserva]   = React.useState(false);
    const [open, setOpen]                   = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    const [peluqueros, setPeluqueros]   = React.useState([]);
    const [select, setSelection]    = React.useState({rowIds:[]});
    const [confirmDialog, setConfirmDialog] = React.useState(
        {   isOpen: false,
            title: '',
            subTitle: '' })

    useEffect(() => {effectCargarPeluqueros();}, []);

    const effectCargarPeluqueros = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getPeluqueros', config)
        .then(result => {
            setPeluqueros(Peluquero.listarPeluquerosFull(result.data.Peluqueros));
        })
        .catch( err => console.log(err));
    }

    const deletePeluqueros = (seleccion) => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                                    }
                            };

        let datos = {peluqueros_id: seleccion}
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        api.post('deletePeluquero', datos, config)
        .then(result => {
            if(result.data.Message === "ERROR"){
                setExitoReserva(false)
                abrirAlerta()
            }else{
                setExitoReserva(true)
                abrirAlerta()
                effectCargarPeluqueros()
            }
        })
        .catch( err => console.log(err));
    }

    const handleEliminar = (seleccion) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        deletePeluqueros(seleccion.rowIds)
    }

    return (
        <React.Fragment>
            <Grid container spacing={1} justify="center">
                <Alertas open={open} cerrar={CerrarAlerta} success={exitoReserva}/>
                <Grid item xs={3}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            if(select.rowIds.length > 0)
                                setConfirmDialog({
                                    isOpen: true,
                                    title: "¿Desea realizar la Eliminación?",
                                    subTitle: "Se eliminaran las reservas tambien, y no puedes deshacerlo luego de confirmar.",
                                    onConfirm: () => handleEliminar(select)
                                })
                        }}
                        className={classes.boton}
                    >
                        Eliminar
                    </Button>
                </Grid> 
                <Grid item xs={12}>
                    <div style={{ height: 450, width: '100%' }}>
                        <DataGrid 
                            rows={peluqueros} 
                            columns={columns} 
                            pageSize={6} 
                            checkboxSelection 
                            onSelectionChange={(newSelection) => {
                                setSelection(newSelection);
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </React.Fragment>
    );
}

export function Eliminar_Peluqueros_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Eliminar_Peluqueros")}>
            <ListItemIcon>
                <PeopleAltIcon />
            </ListItemIcon>
            <ListItemText primary="Peluqueros" />
        </ListItem>
    )
}