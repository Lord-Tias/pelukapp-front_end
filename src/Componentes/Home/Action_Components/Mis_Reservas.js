import React, { useEffect }     from 'react';
import Axios                    from 'axios'

import ListItem                 from '@material-ui/core/ListItem';
import ListItemIcon             from '@material-ui/core/ListItemIcon';
import ListItemText             from '@material-ui/core/ListItemText';
import AssignmentIcon           from '@material-ui/icons/Assignment';
import { makeStyles }           from '@material-ui/core/styles';
import Typography               from '@material-ui/core/Typography';
import Paper                    from '@material-ui/core/Paper';
import Grid                     from '@material-ui/core/Grid';

import List                     from '@material-ui/core/List';
import ListItemAvatar           from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction  from '@material-ui/core/ListItemSecondaryAction';
import Avatar                   from '@material-ui/core/Avatar';
import IconButton               from '@material-ui/core/IconButton';
import AlarmSharpIcon           from '@material-ui/icons/AlarmOutlined';
import { green, blue }          from '@material-ui/core/colors';
import DeleteIcon               from '@material-ui/icons/Delete';
import RoomOutlinedIcon         from '@material-ui/icons/RoomOutlined';
import StorefrontOutlinedIcon   from '@material-ui/icons/StorefrontOutlined';
import AccessAlarmOutlinedIcon  from '@material-ui/icons/AccessAlarmOutlined';

import Card                     from '@material-ui/core/Card';
import CardHeader               from '@material-ui/core/CardHeader';
import CardContent              from '@material-ui/core/CardContent';
import CardActions              from '@material-ui/core/CardActions';

import ConfirmDialog            from './Special_Components/Mensaje_Confirmacion'
  
const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },  
    rootCard: {
        maxWidth: 500,
    },
    mediaCard: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    avatarCard: {
        backgroundColor: blue[500],
    },
    TextiItem: {
        marginLeft: '10px'
    }
});

export function Mis_Reservas() {
    const classes = useStyles();

    const [reservas, setReservas]   = React.useState([]);

    const [confirmDialog, setConfirmDialog] = React.useState(
        {   isOpen: false,
            title: '',
            subTitle: '' })


    //Efecto que se ejecuta el montar el componente.
    //  Se cargan los Peluqueros al renderizar.
    useEffect(() => loadReservas(), []);

    //Consulta las reservas del cliente.
    const loadReservas = () => {
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
            Authorization: `Bearer ${token}`,
            Accept: 'application/json' }};
        let api

        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }
        api.get('getClienteReservas', config)
        .then(result => {
            if(result.data.Message === "OK"){
                setReservas(result.data.Datos)
            }
        })
        .catch( err => console.log(err));
    }

    const deleteReservas = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        let token       = sessionStorage.getItem('token')
        const config    = {headers: { 
                                    Authorization: `Bearer ${token}`,
                                    Accept: 'application/json'
                                    }
                            };

        let datos = {id_reserva: id}
        
        let api
        if (process.env.NODE_ENV === 'production') {
            let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
            api             = Axios.create({baseURL : api_url.concat('/api/')})
        }else{
            api             = Axios.create({baseURL : '/api/'})
        }

        
        api.post('deleteReserva', datos, config)
        .then(result => {
            loadReservas()
        })
        .catch( err => console.log(err));
    }

    const listarReservasViejas = () => {
        return(
            <List>
                {reservas.map(elemento=>{
                    if((new Date(elemento.fecha+" "+elemento.fin) - new Date()) < 0){
                        return(
                            <ListItem key={elemento.id}>
                                <ListItemAvatar>
                                    <Avatar>
                                        <AlarmSharpIcon/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={
                                        "Fecha: " + elemento.fecha+
                                        " - Estilista: "+
                                        elemento.peluquero.user.nombre+
                                        " "+
                                        elemento.peluquero.user.apellido
                                    }
                                    secondary={
                                        "Hora de Inicio: " + elemento.inicio+
                                        " - Hora de Fin: " + elemento.fin
                                    }
                                />
                            </ListItem>
                        )
                    }else{ return null}
                })}
            </List>
        );
    }
    const listarReservas = (primero) => {
        let ahora = new Date()
        return(
            <List>
                {reservas.map(elemento=>{
                    if((new Date(elemento.fecha+" "+elemento.fin) - ahora) > 0){
                        if(primero) ahora = new Date("2999-01-01")
                        return(
                            <ListItem key={elemento.id}>
                                <ListItemAvatar>
                                    <Avatar>
                                        <AlarmSharpIcon style={{ color: green[500] }}/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={
                                        "Fecha: " + elemento.fecha+
                                        " - Estilista: "+
                                        elemento.peluquero.user.nombre+
                                        " "+
                                        elemento.peluquero.user.apellido
                                    }
                                    secondary={
                                        "Hora de Inicio: " + elemento.inicio+
                                        " - Hora de Fin: " + elemento.fin
                                    }
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end"
                                                aria-label="delete"
                                                onClick={() => {
                                                    setConfirmDialog({
                                                        isOpen: true,
                                                        title: 'Está seguro que quiere ELIMINAR la reserva?',
                                                        subTitle: "No puedes deshacerlo luego de confirmar.",
                                                        onConfirm: () => deleteReservas(elemento.id) })
                                                }}
                                    >
                                        <DeleteIcon/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        )
                    }else{ return null}
                })}
            </List>
        );
    }

    const listarReservaActiva = () => {
        let user        = JSON.parse(sessionStorage.getItem('user'))
        let elemento;
        for (var i = 0; i < reservas.length; i++) {
            if((new Date(reservas[i].fecha+" "+reservas[i].fin) - new Date()) > 0) {
                elemento = reservas[i];
                break;
            }
        }
        if(elemento){
            return (
                <Card className={classes.rootCard}>
                    <CardHeader
                        avatar={
                            <Avatar aria-label="usuario" className={classes.avatarCard}>{user.nombre[0]+user.apellido[0]}</Avatar>
                        }
                        title={user.nombre+" "+user.apellido}
                        subheader={"Fecha: "+elemento.fecha+" - Hora: "+elemento.inicio}
                    />
                    <CardContent>
                            <List>
                                <ListItem>
                                    <Avatar>
                                        <StorefrontOutlinedIcon/>
                                    </Avatar>
                                    <ListItemText 
                                            className={classes.TextiItem}
                                            primary={elemento.peluquero.empresa.nombre}/>
                                </ListItem>
                                <ListItem   key={elemento.peluquero.empresa.id}
                                            href={"https://maps.google.com/?ll="+elemento.peluquero.empresa.ubicacion.replace(/\s/g, ',')}
                                            button
                                            component="a"
                                            target="_blank"
                                >
                                    <Avatar>
                                        <RoomOutlinedIcon/>
                                    </Avatar>
                                    <ListItemText 
                                            className={classes.TextiItem}
                                            primary='Google Maps'/>
                                </ListItem>
                                <ListItem>
                                    <Grid container spacing={1} align="center">
                                        <Grid item xs={4} align="center">
                                            <Avatar>
                                                <AccessAlarmOutlinedIcon/>
                                            </Avatar>
                                            <ListItemText 
                                                    className={classes.TextiItem}
                                                    primary={elemento.fecha}
                                                    secondary='Fecha'/>
                                        </Grid>
                                        <Grid item xl={4} align="center">
                                            <Avatar>
                                                <AccessAlarmOutlinedIcon/>
                                            </Avatar>
                                            <ListItemText 
                                                    className={classes.TextiItem}
                                                    primary={elemento.inicio}
                                                    secondary='Inicio'/>
                                        </Grid>
                                        <Grid item xs={4} align="center">
                                            <Avatar>
                                                <AccessAlarmOutlinedIcon/>
                                            </Avatar>
                                            <ListItemText 
                                                    className={classes.TextiItem}
                                                    primary={elemento.fin}
                                                    secondary='Fin estimado'/>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                            </List>
                    </CardContent>
                    <CardActions disableSpacing>
                        <Grid container spacing={1} justify="right">
                            <Grid item xs={12} align="center">
                                <IconButton     
                                            aria-label="Borrar reserva"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: 'Está seguro que quiere ELIMINAR la reserva?',
                                                    subTitle: "No puedes deshacerlo luego de confirmar.",
                                                    onConfirm: () => deleteReservas(elemento.id) })
                                            }}
                                >
                                    <DeleteIcon/>
                                </IconButton>
                            </Grid>
                        </Grid>
                    </CardActions>
                </Card>
            );
        }else{
            return(
                <Card className={classes.rootCard}>
                    <CardContent>
                        <Typography variant="h5" color="textSecondary" component="p" align='center'>
                            No hay reservas
                        </Typography>
                    </CardContent>
                </Card>
            );
        }
    }

    return (
        <React.Fragment>
            <Typography component="p" variant="h2" align='center'>
                Proxima reserva
            </Typography>
            <Grid container spacing={3} justify="center">
                <Grid item xs={4}>

                    <Paper className={classes.paper}>
                        {listarReservaActiva()}
                    </Paper>
                </Grid>
                <Grid container spacing={3} justify="center">
                    <Grid item xs={6}>
                        <Typography component="p" variant="h5" align='center'>
                                Reservas anteriores
                        </Typography>
                        <Paper className={classes.paper}>
                            {listarReservasViejas()}
                        </Paper>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography component="p" variant="h5" align='center'>
                                Reservas agendadas
                        </Typography>
                        <Paper className={classes.paper}>
                            {listarReservas(false)}
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </React.Fragment>
    );
}

export function Mis_Reservas_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Mis_Reservas")}>
            <ListItemIcon>
                <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary="Mis Reservas" />
        </ListItem>
    )
}