import React                    from 'react'

import ListItem                 from '@material-ui/core/ListItem';
import ListItemIcon             from '@material-ui/core/ListItemIcon';
import ListItemText             from '@material-ui/core/ListItemText';
import PersonAddTwoToneIcon     from '@material-ui/icons/PersonAddTwoTone';

import RegistroCliente          from '../../Inicio/Registro_Cliente'  

export function Alta_Clientes() {
    return (
        <React.Fragment>
            <RegistroCliente Rol="Alta_Clientes"/>
        </React.Fragment>
    );
}

export function Alta_Clientes_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Alta_Clientes")}>
            <ListItemIcon>
                <PersonAddTwoToneIcon />
            </ListItemIcon>
            <ListItemText primary="Registrar Clientes" />
        </ListItem>
    )
}