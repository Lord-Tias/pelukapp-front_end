import React            from 'react'
import ListItem         from '@material-ui/core/ListItem';
import ListItemIcon     from '@material-ui/core/ListItemIcon';
import ListItemText     from '@material-ui/core/ListItemText';
import Link             from '@material-ui/core/Link';
import { makeStyles }   from '@material-ui/core/styles';
import Typography       from '@material-ui/core/Typography';

import InsertChartRoundedIcon from '@material-ui/icons/InsertChartRounded';

function preventDefault(event) {
    event.preventDefault();
}
  
const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
});

export function Estadisticas() {
    const classes = useStyles();
    return (
        <React.Fragment>
            <h2>Estadisticas</h2>
            <Typography component="p" variant="h4">
                $3,024.00
            </Typography>
            <Typography color="textSecondary" className={classes.depositContext}>
                on 15 March, 2019
            </Typography>
            <div>
                <Link color="primary" href="#" onClick={preventDefault}>
                    View balance
                </Link>
            </div>
        </React.Fragment>
    );
}

export function Estadisticas_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Estadisticas")}>
            <ListItemIcon>
                <InsertChartRoundedIcon />
            </ListItemIcon>
            <ListItemText primary="Estadisticas" />
        </ListItem>
    )
}