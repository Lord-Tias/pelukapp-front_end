import React                from 'react'
import ListItem             from '@material-ui/core/ListItem';
import ListItemIcon         from '@material-ui/core/ListItemIcon';
import ListItemText         from '@material-ui/core/ListItemText';
import Typography           from '@material-ui/core/Typography';
import Grid                 from '@material-ui/core/Grid';

import HomeIcon             from '@material-ui/icons/Home';
import HomeCliente          from './Special_Components/Home_Cliente';
import HomePeluquero        from './Special_Components/Home_Peluquero';
import HomeAdministrador    from './Special_Components/Home_Administrador';

export function Home() {
    let user    = JSON.parse(sessionStorage.getItem('user'))

    const PerfilLoad = () => {
        switch (user.rol_id) {
            case 1:
                return(
                    <Grid item xs={12}>
                        <HomeAdministrador />
                    </Grid>
                );
            case 2:
                return(
                    <Grid item xs={12}>
                        <HomePeluquero />
                    </Grid>
                );
            case 3:
                return(
                    <Grid item xs={12}>
                        <HomeCliente />
                    </Grid>
                );
            default:
                return(null);
        }
    }

    return (
        <React.Fragment>
            <Grid container spacing={2} justify="center">
                <Grid item xl={12}>
                    <Typography component="p" variant="h4" align='center'>
                        Reservas de la semana
                    </Typography>
                </Grid>
                {PerfilLoad()}
            </Grid>
        </React.Fragment>
    );
}

export function Home_Item(props) {
    return (
        <ListItem button onClick={() => props.clic("Home")}>
            <ListItemIcon>
                <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Inicio" />
        </ListItem>
    )
}