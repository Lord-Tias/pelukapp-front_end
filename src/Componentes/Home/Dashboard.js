import React            from 'react';
import clsx             from 'clsx';

import CssBaseline      from '@material-ui/core/CssBaseline';
import Drawer           from '@material-ui/core/Drawer';
import AppBar           from '@material-ui/core/AppBar';
import Toolbar          from '@material-ui/core/Toolbar';
import List             from '@material-ui/core/List';
import Typography       from '@material-ui/core/Typography';
import Divider          from '@material-ui/core/Divider';
import IconButton       from '@material-ui/core/IconButton';
import Container        from '@material-ui/core/Container';
import MenuIcon         from '@material-ui/icons/Menu';
import ChevronLeftIcon  from '@material-ui/icons/ChevronLeft';

import Card             from '@material-ui/core/Card';
import CardContent      from '@material-ui/core/CardContent';

import * as Actions     from './Action_Components';
import {Home}           from './Action_Components/Home'

import EspecialBadge    from './Action_Components/Special_Components/Especial_Badge'

import useStyles        from '../../Estilos/Dashboard_Styles';

function getActions(){
    let user = JSON.parse(sessionStorage.getItem('user'))
    return user['rol']['actions']
}

export default function Dashboard(props) {
    const classes             = useStyles();

    let user    = JSON.parse(sessionStorage.getItem('user'))

    const [open, setOpen]     = React.useState(false);
    const handleDrawerOpen    = () => {
        setOpen(true);
    };
    const handleDrawerClose   = () => {
        setOpen(false);
    };

    const Actions_Secundarios = ["Actualizar_Password", "Configuraciones", "Logout"]

    //const fixedHeightPaper    = clsx(classes.paper, classes.fixedHeight);

    //let [contenedor, setContenedor]     = React.useState(<Registro />);
    let [contenedor, setContenedor]     = React.useState(<Home key="Home"/>);

    const loadAcction = (e) => {
        const ItemContenedor = Actions[e]
        setContenedor(contenedor = <ItemContenedor key={e}/>)
    }

    const getActionItem     = (nombre) => {
        const ItemMenu = Actions[nombre.concat("_Item")];
        if(ItemMenu == null || Actions_Secundarios.includes(nombre)){
            return null
        }
        else {
            return <ItemMenu key={nombre} clic={(e) => loadAcction(e)}/>
        }
    }
    const cargarItems   = () => {
        const items = getActions().map((element) => 
            getActionItem(element.nombre) 
        )
        return items;
    }

    const getActionItemSecundarios     = (nombre) => {
        const ItemMenu = Actions[nombre.concat("_Item")];
        if(ItemMenu == null){
            return null
        }
        else {
            if(Actions_Secundarios.includes(nombre)){
                return <ItemMenu key={nombre} clic={(e) => loadAcction(e)}/>
            }
            return null
        }
    }
    const cargarItemsSecundarios   = () => {
        const items = getActions().map((element) => 
            getActionItemSecundarios(element.nombre) 
        )
        return items;
    }

    const logout = () => {
        const ItemMenu = Actions["Logout_Item"];
        return <ItemMenu goToExit = {() => props.goToExit()} />
    }

    return (
        <div className={classes.root}>
        <CssBaseline />
            <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
                <Toolbar className={classes.toolbar}>
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                        {user.nombre + ' ' + user.apellido}
                    </Typography>
                    <IconButton color="inherit">
                        <EspecialBadge />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                classes={{
                paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                }}
                open={open}>
                
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <List>
                    {cargarItems()}
                </List>
                <Divider />
                <List>
                    {cargarItemsSecundarios()}
                    {logout()}
                </List>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    <Card >
                        <CardContent>
                            {contenedor}
                        </CardContent>
                    </Card>
                </Container>
            </main>
        </div>
    );
}
