import React, { useState }  from 'react'
import Axios                from 'axios'

import Button               from '@material-ui/core/Button';
import CssBaseline          from '@material-ui/core/CssBaseline';
import TextField            from '@material-ui/core/TextField';
import Grid                 from '@material-ui/core/Grid';
import { makeStyles }       from '@material-ui/core/styles';
import Container            from '@material-ui/core/Container';

import 'react-phone-input-2/lib/material.css'

import TitulosRegistrar     from './Inicio_Componentes/Titulos_Registrar';
import Alertas              from './Inicio_Componentes/Alertas'

const useStyles = makeStyles((theme) => ({
    errorMsg: {
        marginLeft:         '14px',
        marginRight:        '14px',
        color:              '#f44336',
        fontSize:           '0.75rem'
    },
    paper: {
        marginTop:          theme.spacing(2),
        display:            'flex',
        flexDirection:      'column',
        alignItems:         'center',
    },
    avatar: {
        margin:             theme.spacing(1),
        //backgroundColor:    theme.palette.secondary.main,
    },
    form: {
        width:              '100%', // Fix IE 11 issue.
        marginTop:          theme.spacing(3),
    },
    submit: {
        margin:             theme.spacing(3, 0, 2),
    },
}));

export default function Registro_Admin(props) {
    //Instancia de estilos.
    const classes = useStyles();

    //Estados.
    let [nombre             , setNombre]                = useState('')
    let [nombreError        , setNombreError]           = useState('')
    let [apellido           , setApellido]              = useState('')
    let [apellidoError      , setApellidoError]         = useState('')
    let [email              , setEmail]                 = useState('')
    let [emailError         , setEmailError]            = useState('')
    let [password           , setPassword]              = useState('')
    let [passwordError      , setPasswordError]         = useState('')
    let [password_confirmation      , setPassword_Conf]         = useState('')

    const [open, setOpen] = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    //Especificacion del rol a registrar.
    const rol   = props.Rol

    //Instancias de Axios para consumir la API.
    let api
    if (process.env.NODE_ENV === 'production') {
        let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
        api             = Axios.create({baseURL : api_url.concat('/api/')})
    }else{
        api             = Axios.create({baseURL : '/api/'})
    }
    let token           = sessionStorage.getItem('token')
    const config        = {
        headers: { Authorization: `Bearer ${token}`, Accept: 'application/json' }
    };

    //Se borran los mensajes de error.
    const cleanErrors = () => {
        setNombreError(nombreError      = '')
        setApellidoError(apellidoError  = '')
        setEmailError(emailError        = '')
        setPasswordError(passwordError  = '')
    }

    //Se colocan los mensajes de error.
    const setMensagesError = (Msg) => {
        cleanErrors()
        setNombreError(nombreError      = Msg.nombre)
        setApellidoError(apellidoError  = Msg.apellido)
        setEmailError(emailError        = Msg.email)
        setPasswordError(passwordError  = Msg.contraseña)
    }

    //Se borran los campos de registro
    const cleanCamposRegistro = () => {
        setNombre(nombre                        = '')
        setApellido(apellido                    = '')
        setEmail(email                          = '')
        setPassword(password                    = '')
        setPassword_Conf(password_confirmation  = '')
    }
    

    //Se ejecuta un Post de registro a la API.
    const onSubmit = (e) => {
        e.preventDefault()
        RegFunction()
    };
    const RegFunction = async () => {
        let respuesta = await api.post(
            'registro',
             {  nombre:             nombre,
                apellido:           apellido,
                email:              email,
                contraseña:         password,
                contraseña_confirmation:    password_confirmation,
            },
            config)
        if(respuesta.data.Message === "ERROR"){
            setMensagesError(respuesta.data.Errores)
        }else{
            cleanCamposRegistro()
            cleanErrors()
            abrirAlerta()
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <TitulosRegistrar Rol={rol} />
                <Alertas open={open} cerrar={CerrarAlerta}/>
                <form   className={classes.form}
                        onSubmit={onSubmit} noValidate autoComplete="off"
                >
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete    = "fname"
                                name            = "nombre"
                                variant         = "outlined"
                                required
                                fullWidth
                                id              = "nombre"
                                label           = "Nombre"
                                autoFocus
                                onChange={(e) => setNombre(
                                    nombre = e.target.value
                                )}
                                value={nombre}
                            />
                            <p className={classes.errorMsg}>
                                {nombreError}</p>
                        </Grid><Grid item xs={12}>
                            <TextField
                                autoComplete    = "fname"
                                name            = "apellido"
                                variant         = "outlined"
                                required
                                fullWidth
                                id              = "apellido"
                                label           = "Apellido"
                                onChange={(e) => setApellido(
                                    apellido = e.target.value
                                )}
                                value={apellido}
                            />
                            <p className={classes.errorMsg}>
                                {apellidoError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                id              = "email"
                                label           = "Email"
                                name            = "email"
                                autoComplete    = "email"
                                onChange={(e) => setEmail(
                                    email = e.target.value
                                )}
                                value={email}
                            />
                            <p className={classes.errorMsg}>
                                {emailError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                name            = "password"
                                label           = "Password"
                                type            = "password"
                                id              = "password"
                                onChange={(e) => setPassword(
                                    password = e.target.value
                                )}
                                value={password}
                            />
                            <p className={classes.errorMsg}>
                                {passwordError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                    variant         = "outlined"
                                    required
                                    fullWidth
                                    name            = "password_confirmation"
                                    label           = "Confirmar Password"
                                    type            = "password"
                                    id              = "password_confirmation"
                                    onChange={(e) => setPassword_Conf(
                                        password_confirmation = e.target.value
                                    )}
                                    value={password_confirmation}
                                />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Registrar Usuario
                    </Button>
                </form>
            </div>
        </Container>
    );
}