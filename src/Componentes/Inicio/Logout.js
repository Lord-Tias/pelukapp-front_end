import React                from 'react'
import Axios                from 'axios'

import ListItem             from '@material-ui/core/ListItem';
import ListItemIcon         from '@material-ui/core/ListItemIcon';
import ListItemText         from '@material-ui/core/ListItemText';

import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded';

export function Logout(token) {
    //Instancia de Axios para conexion de API.
    let api
    if (process.env.NODE_ENV === 'production') {
        let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
        api             = Axios.create({baseURL : api_url.concat('/api/')})
    }else{
        api     = Axios.create({baseURL : '/api/'})
    }

    const config = {
        headers: { Authorization: `Bearer ${token}` }
    };

    let respuesta = ""
    try {
        respuesta = api.post(
                'logout',
                null,
                config)
    } catch (error) {
        console.log(error)
        console.log(respuesta)
    }
}

export function Logout_Item(props) {
    return (
        <ListItem button onClick={() => props.goToExit()}>
            <ListItemIcon>
                <ExitToAppRoundedIcon />
            </ListItemIcon>
            <ListItemText primary="Salir" />
        </ListItem>
    )
}