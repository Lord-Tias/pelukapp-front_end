import React            from 'react';
import { makeStyles }   from '@material-ui/core/styles';
import AppBar           from '@material-ui/core/AppBar';
import Toolbar          from '@material-ui/core/Toolbar';
import Typography       from '@material-ui/core/Typography';

import BotonMenuInicio  from './BotonMenuInicio'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow:       1,
    },
    menuButton: {
        marginRight:    theme.spacing(2),
    },
    title: {
        flexGrow:       1,
    },
}));

export default function ButtonAppBar(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <BotonMenuInicio 
                        goToLogin       = {() => props.goToLogin()}
                        goToRegistro    = {() => props.goToRegistro()}
                        goToInfo        = {() => props.goToInfo()}
                    />
                    <Typography variant="h6" className={classes.title}>
                        PeluKApp
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}
