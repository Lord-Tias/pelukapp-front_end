import React, { useState }  from 'react'
import Axios                from 'axios'

import Avatar 				from '@material-ui/core/Avatar';
import Button 				from '@material-ui/core/Button';
import CssBaseline 			from '@material-ui/core/CssBaseline';
import TextField 			from '@material-ui/core/TextField';
import Link 				from '@material-ui/core/Link';
import Grid 				from '@material-ui/core/Grid';
import LockOutlinedIcon		from '@material-ui/icons/LockOutlined';
import Typography 			from '@material-ui/core/Typography';
import Container 			from '@material-ui/core/Container';

import { makeStyles }       from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    errorMsg: {
        marginLeft:         '14px',
        marginRight:        '14px',
        color:              '#f44336',
        fontSize:           '1rem',
        textAlign:          'center'
    },
    avatar: {
        margin: theme.spacing(1),
        //backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
  }));


export default function Login(props) {
    //Estados.
    let [email      , setEmail]     = useState('')
    let [password   , setPassword]  = useState('')
    let [errorMSG   , seterrorMSG]  = useState('')

    //Instancia de Axios para conexion de API.
    let api
    if (process.env.NODE_ENV === 'production') {
        let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
        api             = Axios.create({baseURL : api_url.concat('/api/')})
    }else{
        api     = Axios.create({baseURL : '/api/'})
    }

    //Instancia de estilos.
    const classes   = useStyles();

    //Funciones por parametro de App.
    const goToHome      = props.goToHome;
    const goToRegistro  = props.goToRegistro;

    //Funcion que guarda datos de login en Session Storage.
    const SaveSession = (datos) => {
        sessionStorage.setItem('token', datos.Token)
        sessionStorage.setItem('user', JSON.stringify(datos.Usuario))
        sessionStorage.setItem('rol_info', JSON.stringify(datos.Rol_Info))
    }

    //Funcion de Login.
    const LogFunction = async () => {
        try {
            let respuesta = await api.post(
                'login',
                 {email: email, password: password})
    
            if(respuesta.status === 200){
                if(respuesta.data.Message === "ERROR"){
                    seterrorMSG(errorMSG=respuesta.data.Errores)
                }
                else if(respuesta.data.Message === "OK"){
                    SaveSession(respuesta.data)
                    goToHome()
                }
                else{
                    seterrorMSG(
                        errorMSG='Error con el servidor. Intentalo mas tarde.')
                }
            }else{
                seterrorMSG(
                    errorMSG='Error con el servidor. Intentalo mas tarde.')
            }
        } catch (error) {
            seterrorMSG(
                errorMSG='Error con el servidor. Intentalo mas tarde.')
        }
        
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Login
                </Typography>
                <form className={classes.form}
                        onSubmit={(e) => {
                            e.preventDefault()
                            LogFunction()
                        }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="E-mail"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        onChange={(e) => setEmail(
                            email = e.target.value
                        )}
                        onFocus={(e) => seterrorMSG(
                            errorMSG=''
                        )}
                        value={email}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={(e) => setPassword(
                            password = e.target.value
                        )}
                        onFocus={(e) => seterrorMSG(
                            errorMSG=''
                        )}
                        value={password}
                    />
                    <p className={classes.errorMsg}>
                        {errorMSG}</p>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        >
                        Entrar
                    </Button>
                    <Grid container className='grilla'>
                        <Grid item>
                            <Link onClick={() => goToRegistro()} variant="body2">
                                ¿No tienes cuenta? Crear una.
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    )
}
