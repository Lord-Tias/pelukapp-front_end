import React from 'react'

import Avatar                   from '@material-ui/core/Avatar';
import Typography               from '@material-ui/core/Typography';
import { makeStyles }           from '@material-ui/core/styles';

import PersonAddOutlinedIcon    from '@material-ui/icons/PersonAddOutlined';
import PersonAddRoundedIcon     from '@material-ui/icons/PersonAddRounded';
import PersonAddTwoToneIcon     from '@material-ui/icons/PersonAddTwoTone';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop:          theme.spacing(1),
        display:            'flex',
        flexDirection:      'column',
        alignItems:         'center',
    },
    avatar: {
        margin:             theme.spacing(1),
        //backgroundColor:    theme.palette.secondary.main,
    }
}));

export default function Titulos_Registrar(props) {

    //Rol del cual se va a registrar
    const rol = props.Rol

    //Instancia de estilos.
    const classes = useStyles();

    const getComponente = () => {
        switch (rol) {
            case "Alta_Administradores":
                return(
                    <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <PersonAddOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Registrar Administrador
                    </Typography>
                </div>
                )
            case "Alta_Peluqueros":
                return(
                    <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <PersonAddRoundedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Registrar Peluquero
                    </Typography>
                </div>
                )
            case "Alta_Clientes":
                return(
                    <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <PersonAddTwoToneIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Registro de Cliente
                    </Typography>
                </div>
                )
            case "Inicio":
                return(
                    <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <PersonAddTwoToneIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Registro de Usuario
                    </Typography>
                </div>
                )
            default:
                break;
        }
    }

    return (
        getComponente()
    )
}
