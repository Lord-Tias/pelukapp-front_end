import React                    from 'react';

import Button                   from '@material-ui/core/Button';
import Menu                     from '@material-ui/core/Menu';
import MenuItem                 from '@material-ui/core/MenuItem';
import MenuIcon                 from '@material-ui/icons/Menu';
import Link 	                from '@material-ui/core/Link';
import Grid 		            from '@material-ui/core/Grid';
import { Divider }              from '@material-ui/core';

import LockOutlinedIcon		    from '@material-ui/icons/LockOutlined';
import PersonAddOutlinedIcon    from '@material-ui/icons/PersonAddOutlined';
//import InfoOutlinedIcon         from '@material-ui/icons/InfoOutlined';

import { makeStyles }           from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    item_menu: {
        color: '#000000'
    },
}));


export default function BotonMenuInicio(props) {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const goToLogin       = props.goToLogin
    const goToRegistro    = props.goToRegistro
    //const goToInfo        = props.goToInfo

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

  return (
        <div>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                <MenuIcon />
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem>
                    MENU
                <Divider absolute />
                </MenuItem>
                <MenuItem onClick={handleClose}>
                    <Link   className={classes.item_menu}
                            underline='none'
                            onClick={() => goToLogin()}>
                        <Grid container spacing={5}>
                            <Grid item xs={1}>
                                <p><LockOutlinedIcon /></p>
                            </Grid>
                            <Grid item xs={6}>
                                <p>Login</p>
                            </Grid>
                        </Grid>
                    </Link>
                </MenuItem>
                <MenuItem onClick={handleClose}>
                    <Link   className={classes.item_menu}
                            underline='none'
                            onClick={() => goToRegistro()}>
                        <Grid container spacing={5}>
                            <Grid item xs={1}>
                                <p><PersonAddOutlinedIcon/></p>
                            </Grid>
                            <Grid item xs={6}>
                                <p>Registro de Usuario</p>
                            </Grid>
                        </Grid>
                    </Link>
                </MenuItem>
            </Menu>
        </div>
  );
}