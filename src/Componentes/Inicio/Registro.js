import React, { useState }  from 'react'
import Axios                from 'axios'

import Button               from '@material-ui/core/Button';
import CssBaseline          from '@material-ui/core/CssBaseline';
import TextField            from '@material-ui/core/TextField';
import Link                 from '@material-ui/core/Link';
import Grid                 from '@material-ui/core/Grid';
import { makeStyles }       from '@material-ui/core/styles';
import Container            from '@material-ui/core/Container';


import PhoneInput           from 'react-phone-input-2'

import 'react-phone-input-2/lib/material.css'

import TitulosRegistrar     from './Inicio_Componentes/Titulos_Registrar';
import Alertas              from './Inicio_Componentes/Alertas'

const useStyles = makeStyles((theme) => ({
    errorMsg: {
        marginLeft:         '14px',
        marginRight:        '14px',
        color:              '#f44336',
        fontSize:           '0.75rem'
    },
    paper: {
        marginTop:          theme.spacing(2),
        display:            'flex',
        flexDirection:      'column',
        alignItems:         'center',
    },
    avatar: {
        margin:             theme.spacing(1),
        //backgroundColor:    theme.palette.secondary.main,
    },
    form: {
        width:              '100%', // Fix IE 11 issue.
        marginTop:          theme.spacing(3),
    },
    submit: {
        margin:             theme.spacing(3, 0, 2),
    },
}));

export default function Registro(props) {
    //Instancia de estilos.
    const classes = useStyles();

    //Estados.
    let [nombre             , setNombre]                = useState('')
    let [nombreError        , setNombreError]           = useState('')
    let [email              , setEmail]                 = useState('')
    let [emailError         , setEmailError]            = useState('')
    let [password           , setPassword]              = useState('')
    let [passwordError      , setPasswordError]         = useState('')
    let [password_confirmation      , setPassword_Conf]         = useState('')
    let [telefono           , setTelefono]              = useState('')
    let [telefonoError      , setTelefonoError]         = useState('')

    const [open, setOpen] = React.useState(false);
    const abrirAlerta = () => {
        setOpen(true);
    }
    const CerrarAlerta = () => {
        setOpen(false);
    }

    //Especificacion del rol a registrar.
    const rol   = props.Rol
    const roles = {
        Alta_Administradores:   1,
        Alta_Peluqueros:        2,
        Alta_Cliente:           3,
        Inicio:                 3
    }

    //Funcion recibida por propiedad para ir a Login.
    const goToLogin = props.goToLogin

    //Instancias de Axios para consumir la API.
    let api
    if (process.env.NODE_ENV === 'production') {
        let api_url     = process.env.REACT_APP_PRODUCCTION_API_URL
        api             = Axios.create({baseURL : api_url.concat('/api/')})
    }else{
        api     = Axios.create({baseURL : '/api/'})
    }

    //Se borran los mensajes de error.
    const cleanErrors = () => {
        setNombreError(nombreError = '')
        setEmailError(emailError = '')
        setPasswordError(passwordError = '')
        setTelefonoError(telefonoError = '')
    }

    //Se colocan los mensajes de error.
    const setMensagesError = (Msg) => {
        cleanErrors()
        setNombreError(nombreError = Msg.nombre)
        setEmailError(emailError = Msg.email)
        setPasswordError(passwordError = Msg.contraseña)
        setTelefonoError(telefonoError = Msg.telefono)
    }

    //Se borran los campos de registro
    const cleanCamposRegistro = () => {
        setNombre(nombre = '')
        setEmail(email = '')
        setPassword(password = '')
        setPassword_Conf(password_confirmation = '')
        setTelefono(telefono = '')
    }
    

    //Se ejecuta un Post de registro a la API.
    const onSubmit = (e) => {
        e.preventDefault()
        RegFunction()
    };
    const RegFunction = async () => {
        let respuesta = await api.post(
            'registro',
             {  nombre:             nombre,
                email:              email,
                contraseña:         password,
                contraseña_confirmation:    password_confirmation,
                telefono:           telefono,
                rol:                roles[rol]
            })

        if(respuesta.data.Message === "ERROR"){
            setMensagesError(respuesta.data.Errores)
        }else{
            cleanCamposRegistro()
            abrirAlerta()
        }
    }

    const getLinkToLogin = () => {
        switch (rol) {
            case "Inicio":
                return(
                    <Grid item>
                        <Link onClick={() => goToLogin()} variant="body2">
                            Ir a Login
                        </Link>
                    </Grid>
                )
            default:
                return null
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <TitulosRegistrar Rol={rol} />
                <Alertas open={open} cerrar={CerrarAlerta}/>
                <form   className={classes.form}
                        onSubmit={onSubmit} noValidate autoComplete="off"
                >
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete    = "fname"
                                name            = "nombre"
                                variant         = "outlined"
                                required
                                fullWidth
                                id              = "nombre"
                                label           = "Nombre y apellido"
                                autoFocus
                                onChange={(e) => setNombre(
                                    nombre = e.target.value
                                )}
                                value={nombre}
                            />
                            <p className={classes.errorMsg}>
                                {nombreError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                id              = "email"
                                label           = "Email"
                                name            = "email"
                                autoComplete    = "email"
                                onChange={(e) => setEmail(
                                    email = e.target.value
                                )}
                                value={email}
                            />
                            <p className={classes.errorMsg}>
                                {emailError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant         = "outlined"
                                required
                                fullWidth
                                name            = "password"
                                label           = "Password"
                                type            = "password"
                                id              = "password"
                                onChange={(e) => setPassword(
                                    password = e.target.value
                                )}
                                value={password}
                            />
                            <p className={classes.errorMsg}>
                                {passwordError}</p>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                    variant         = "outlined"
                                    required
                                    fullWidth
                                    name            = "password_confirmation"
                                    label           = "Confirmar Password"
                                    type            = "password"
                                    id              = "password_confirmation"
                                    onChange={(e) => setPassword_Conf(
                                        password_confirmation = e.target.value
                                    )}
                                    value={password_confirmation}
                                />
                        </Grid>
                        <Grid item xs={12}>
                            <PhoneInput
                                country             = {'uy'}
                                specialLabel        = "Teléfono"
                                inputProps          = {{
                                    name:       'Telefono',
                                    required:   true,
                                    label:      'Telefono',
                                    style:      {width: "100%", position: "relative"}
                                }}
                                onChange={(value, country, e, formattedValue) => setTelefono(
                                    telefono = value
                                )}
                                value={telefono}
                            />
                            <p className={classes.errorMsg}>
                                {telefonoError}</p>
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Registrar Usuario
                    </Button>
                    <Grid container justify="flex-end">
                        {getLinkToLogin()}
                    </Grid>
                </form>
            </div>
        </Container>
    );
}