import React, { Component } from 'react'

import Login                from './Inicio/Login'
import RegistroCliente      from './Inicio/Registro_Cliente';
import Info                 from './Inicio/Info'
import NavInicio            from './Inicio/NavInicio'
import Dashboard            from './Home/Dashboard';
import {Logout}             from './Inicio/Logout'

import '../Estilos/App.css';

export default class App extends Component {

    //Constructor
    constructor(props){
        super(props);
        if(sessionStorage.getItem('token') == null){
            this.state = {
                componente : <div>
                                {this.getNavInicio()}
                                <Login  goToHome     = {this.goToHome}
                                        goToRegistro = {this.goToRegistro}
                                />
                            </div>
            }
        }else{
            this.state = {
                componente : <div>
                                <Dashboard goToExit = {this.goToExit}/>
                            </div>
            }
        }
    };

    //Funcion para cargar dinamicamente la barra
    // de navegacion con las propiedades.
    getNavInicio = () => {
        return  <NavInicio 
                    goToLogin       = {this.goToLogin}
                    goToRegistro    = {this.goToRegistro}
                    goToInfo        = {this.goToInfo}
                />
    }

    //Funcion para cargar dinamicamente el componente Dashboard.
    goToHome = () => {
        const isLoggedIn = this.isLoggedIn();
        if (isLoggedIn){
            this.setState({
                componente : <div>
                                <Dashboard goToExit = {this.goToExit}/>
                            </div>
              })
        }
    }

    //Funcion para cargar dinamicamente el componente Login.
    goToLogin = () => {
        this.setState({
            componente : <div>
                            {this.getNavInicio()}
                            <Login  goToHome     = {this.goToHome}
                                    goToRegistro = {this.goToRegistro}
                            />
                        </div>
          })
        this.goToHome()
    }

    //Funcion para cargar dinamicamente el componente Registro.
    goToRegistro = () => {
        this.setState({
            componente : <div>
                            {this.getNavInicio()}
                            <RegistroCliente   goToLogin = {this.goToLogin}
                                                Rol="Inicio"
                            />
                        </div>
          })
    }

    //Funcion para cargar dinamicamente el componente Informacion.
    goToInfo = () => {
        this.setState({
            componente : <div>
                            {this.getNavInicio()}
                            <Info />
                        </div>
          })
    }

    goToExit = () => {
        Logout(sessionStorage.getItem('token'))
        sessionStorage.clear()
        this.goToLogin()
    }

    //Funcion para verificar si hay una sesión registrada. 
    isLoggedIn(){
        let token = sessionStorage.getItem('token')
        if(token === null) return false
        else return true
    }

    //Renderizacion del componente.
    render() {
        return (
            this.state.componente
        )
    }
}