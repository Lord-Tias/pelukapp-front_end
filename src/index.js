import React			from 'react';
import ReactDOM 		from 'react-dom';
import App 				from './Componentes/App';

import './Estilos/index.css';

ReactDOM.render(
  //<React.StrictMode>
    <App />,
  //</React.StrictMode>,
  document.getElementById('root')
);